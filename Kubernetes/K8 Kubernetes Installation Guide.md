# Installation Guide for K8 & K9 Kubernetes

This is comprehensive guide to get K8s started. It also provides an installation guide for all tools required.

## Table of Contents

1. [Install Chocolatey](#install-chocolatey)
2. [Install kubectl with Chocolatey](#install-kubectl-with-chocolatey)
3. [Install kubelogin with Chocolatey](#install-kubelogin-with-chocolatey)
4. [Install k9s with Chocolatey](#install-k9s-with-chocolatey)
5. [Install OpenLens with Chocolatey](#install-openlens-with-chocolatey)
5. [Configuration](#configuration)

## **Install Chocolatey**

Website: https://chocolatey.org/install

Open an administrative shell (powershell)

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

Test if it works

```
choco

Chocolatey v1.3.1
Please run 'choco -?' or 'choco <command> -?' for help menu.
```

Useful commands

```
choco list
choco outdated
choco upgrade all
```

---

## **Install kubectl with Chocolatey**

Website: https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/#install-nonstandard-package-tools

Open a administrative shell (powershell)

```
choco install kubernetes-cli
```

Test if it works

```
kubectl version --client

WARNING: This version information is deprecated and will be replaced with the output from kubectl version --short.  Use --output=yaml|json to get the full version.
Client Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.3", GitCommit:"9e644106593f3f4aa98f8a84b23db5fa378900bd", GitTreeState:"clean", BuildDate:"2023-03-15T13:40:17Z", GoVersion:"go1.19.7", Compiler:"gc", Platform:"windows/amd64"}
Kustomize Version: v4.5.7
```

Useful commands

```
kubectl get pod                             -->      Show pods
kubectl get pod -o wide                     -->      Show pods with more details
kubectl delete pod "t5-4124124"             -->      Terminate pod
kubectl logs t5-4124124                     -->      Show pods logs
kubectl get svc                             -->      Show services (reachable internally)
kubectl describe svc name-service           -->      Describe services (info related to endpoint etc.)
kubectl get rs                              -->     Show replicas (replica set always created with update for rollback)
kubectl get ingress                         -->     Show Ingress (reverse Proxy..)
kubectl describe ingress name-ingress       -->     Describe Ingress (more info..)
kubectl get cert                            -->     Show certificates and requests
kubectl get certificaterequests             -->     Show requests over certificates
kubectl get secrets                         -->     Show secrets
kubectl get deployment name-yaml            -->     Get yaml file of current deployment
kubectl config get-contexts                 -->     Show all contexts
kubectl config use-contexts CONTEXT_NAME    -->     Switch context
```

---

## **Install kubelogin with Chocolatey**

Website: https://github.com/int128/kubelogin

Open a administrative shell (powershell)

```
choco install kubelogin
```

---

## **Install k9s with Chocolatey**

Tool for visualization of kubernetes clusters

Website: https://k9scli.io/

Open an administrative shell (powershell)

```
choco install k9s
```

Test if it works

```
k9s
```

![K9s main menu](https://k9scli.io/assets/screens/pods.png)

Useful commands

```
Switch views with ':'

:context        -->      list all Contexts to change Cluster
:namespace      -->      list all Namespaces
:pod            -->      list all Pods
:pvc            -->      list all Persistentvolumes
:q              -->      Quit
:deployments    -->      Show list of deployments
```

General

```
<?>           -->      Help
</term>       -->      Filter mode example '/podname'
<enter>       -->      View
<esc>         -->      Back/Clear

with 'l' you can see the logs
with 's' you can start a shell
```

---

## **Install OpenLens with Chocolatey**

Tool for visualization of kubernetes clusters.

Website: [Release v6.0.0 · MuhammedKalkan/OpenLens · GitHub](https://github.com/MuhammedKalkan/OpenLens/releases/tag/v6.0.0)

Open an administrative shell (powershell)

```
choco install openlens
```

Start OpenLens via Program menu in Windows

Add clusters → Click on "plus" icon and select "Sync Kubeconfig file(s)" and select your 'config' file under `C:\Users\<your user>\.kube`

Click on a cluster to connect

![Open lens main menu](https://opensource.com/sites/default/files/uploads/lens_5-pods.png)

---

## **Configuration**

If you don't have folder `.kube` inside of C:\Users\<USER.NAME>\ - then you need to create one.

And then create additional configuration file for kubernetes - `C:\Users\<USER.NAME>\.kube\config.config`

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: <VALID_CERTIFICATE>
    server: https://SERVER_ADDRESS_DEV:PORT
  name: DEV
- cluster:
    certificate-authority-data: <VALID_CERTIFICATE>
    server: https://SERVER_ADDRESS_TEST:PORT
  name: TEST
- cluster:
    certificate-authority-data: <VALID_CERTIFICATE>
    server: https://SERVER_ADDRESS_PROD:PORT
  name: PROD
contexts:
- context:
    cluster: SERVER_ADDRESS_DEV
    namespace: NAMESPACE_DEV
    user: ibrahim.izirov
  name: namespace-dev
- context:
    cluster: SERVER_ADDRESS_TEST
    namespace: NAMESPACE_TEST
    user: ibrahim.izirov
  name: namespace-test
- context:
    cluster: SERVER_ADDRESS_PROD
    namespace: NAMESPACE_PROD
    user: ibrahim.izirov
  name: namespace-prod
kind: Config
preferences: {}
users:
- name: ibrahim.izirov
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      args:
      - oidc-login
      - get-token
      - --oidc-issuer-url=ISSUER_URL_TO_KEYCLOACK_REALM
      - --oidc-client-id=kubernetes_v3
      command: kubectl
      env: null
```

---