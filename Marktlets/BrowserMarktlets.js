+-------------------------------------------------------------------------------------------+
| // Remove hovered DOM elements with a click and close the script with the Esc button. 	|
+-------------------------------------------------------------------------------------------+
javascript:if (!dieFunc) {
    var dieFunc = function () {
        var curElem = undefined;
        var stl = document.createElement('style');
        var txtnode = document.createTextNode('body:hover .coupontooltip {display: block;} .coupontooltip {display: none; background: #C8C8C8; margin-left: 5px; padding: 0px; position: absolute; z-index: 1000;} .onhvrbrd {border-style: solid; border-width: 1px; border-color:red}');
        stl.appendChild(txtnode);
        document.getElementsByTagName('head')[0].appendChild(stl);
        tooltiper = document.createElement('div');
        tooltiper.className = 'coupontooltip';
        tooltiper.Id = 'coupontooltip';
        document.getElementsByTagName('body')[0].appendChild(tooltiper);
        document.onmousemove = function (e) {
            tooltiper.style.left = e.pageX + 'px';
            tooltiper.style.top = e.pageY + 'px';
            tooltiper.innerHTML = 'Tag Type: ' + e.composedPath()[0].tagName + '';
            for (var i = 0; i < e.composedPath()[0].attributes.length; i++) {
                tooltiper.innerHTML += '<br>' + e.composedPath()[0].attributes[i].name + ': ' + e.composedPath()[0].attributes[i].value;
            }
        };
        document.onmouseover = function (e) {
            e.composedPath()[0].classList.add('onhvrbrd');
            curElem = e.composedPath()[0];};
        document.onmouseout = function (e) {
            e.composedPath()[0].classList.remove('onhvrbrd');};
        document.onclick = function (e) {
            e.composedPath()[0].style.display = 'none';};
        document.onkeyup = function (e) {
            e = e || window.event;
            if (e.key === 'Escape') {
                dieFunc = undefined;
                document.onmousemove = null;
                document.onmouseover = null;
                document.onmouseout = null;
                document.onclick = null;
                document.onkeyup = null;
                var thetools = document.getElementsByClassName('coupontooltip');
                for (var i = 0; i < thetools.length; i++) {
                    thetools[i].parentElement.removeChild(thetools[i]);}
                stl.parentElement.removeChild(stl);
                curElem = null;
                tooltiper = null;
            }
        };
    };
    dieFunc();
} else {
    dieFunc = undefined;
    document.onmousemove = null;
    document.onmouseover = null;
    document.onmouseout = null;
    document.onclick = null;
    document.onkeyup = null;
    var thetools = document.getElementsByClassName('coupontooltip');
    for (var i = 0; i < thetools.length; i++) {
        thetools[i].parentElement.removeChild(thetools[i]);
    }
}

document.body.focus();


+-----------------------------------------------------------------------------------+
| // Generate password with usable lenght and copy it directly to the clipboard 	|
+-----------------------------------------------------------------------------------+
javascript:void((function() {
  const upperCase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowerCase = 'abcdefghijklmnopqrstuvwxyz';
  const digits = '0123456789';
  const special = '!#$*-';
  const passwordLength = parseInt(prompt('Enter password length', '12'), 10);

  const allChars = [
    upperCase,
    lowerCase,
    digits,
    special
  ].join('');

  let password = '';
  for (let i = 0; i < passwordLength; i++) {
    password += allChars.charAt(Math.floor(Math.random() * allChars.length));
  }

  const copyToClipboard = (text) => {
    const textarea = document.createElement('textarea');
    textarea.textContent = text;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand('copy');
    document.body.removeChild(textarea);
  };

  copyToClipboard(password);
  alert('Your password has been copied to the clipboard:\n' + password);
})())


+---------------------------------------------------------------------------------------+
| // Make password visible on the page for which you have password field (**********) 	|
+---------------------------------------------------------------------------------------+
javascript: (function() {
    var inputs = document.querySelectorAll('input[type="password"]');
    inputs.forEach(function(input) {
        input.type = "text";
    });
})();


+---------------------------------------------------------------------------------------+
| // Shows you number of words, char count and other information on highlighted text. 	|
+---------------------------------------------------------------------------------------+
javascript:(function() {
    function cleanAndSplitText(text) {
        return text && text !== "" ? text.trim().replace(/\s+/gi, " ").replace(/[^\w\s]/gi, "").split(" ") : [];
    }

    function analyzeText(selectedText) {
        var words = cleanAndSplitText(selectedText) || [];
        var wordCount = words.length;
        var charCount = selectedText.length;

        if (wordCount === 0) return null;

        var totalLength = 0;
        words.forEach(function(word) {
            totalLength += word.length;
        });

        var avgLen = totalLength / wordCount;

        return {
            wordCount: wordCount,
            charCount: charCount,
            avgLen: avgLen
        };
    }

    var analyticsBox = document.createElement("div");
    analyticsBox.setAttribute("style", "color:black; position:fixed; left:5%; top:5%; width:150px; height:auto; font-size:14px; z-index:10000; background:white; border-radius:0px; border:1px solid grey; padding:5px 2px 5px 4px;");
    analyticsBox.setAttribute("id", "analyticsbox");

    var selectedText = "";
    if (window.getSelection) {
        selectedText = window.getSelection().toString();
    }

    var analysis = analyzeText(selectedText);

    var analysisResult = document.createElement("div");
    analysisResult.setAttribute("style", "margin:5px; font-family:serif; line-height:1.2em; font-size:16px;");

    if (analysis === null) {
        analysisResult.innerHTML = "No text selected.<br/>Select some text and click the bookmark to run text analysis.";
    } else {
        analysisResult.innerHTML = "Words: " + analysis.wordCount + "<br/>";
        analysisResult.innerHTML += "Chars: " + analysis.charCount + "<br/><br/>";
        analysisResult.innerHTML += "Avg Word Length: " + analysis.avgLen.toFixed(2) + "<br/>";
    }

    analyticsBox.appendChild(analysisResult);

    var closeButton = document.createElement("div");
    closeButton.setAttribute("style", "color:black; position:absolute; right:6px; top:5px; width:auto; font-size:14px; text-align:right; font-family:Arial, sans-serif; cursor:pointer;");
    closeButton.appendChild(document.createTextNode("X"));
    closeButton.setAttribute("id", "textAnalyticsClose");
    analyticsBox.appendChild(closeButton);

    closeButton.addEventListener("click", function() {
        analyticsBox.style.display = "none";
    }, false);

    document.body.appendChild(analyticsBox);
})();


+-------------------------------------------+
| // Remove StackOverflow page side bars  	|
+-------------------------------------------+
javascript: (function () {
	const sidepanelDiv = document.getElementById('sidebar');
	const sidepanelDivLeft = document.getElementById('left-sidebar');
	const mainDiv = document.getElementById('mainbar');
	sidepanelDiv.hidden = !sidepanelDiv.hidden;
	sidepanelDivLeft.hidden = !sidepanelDivLeft.hidden;

	if (mainDiv.style.width === '100%') {
		mainDiv.style.width = 'calc(100% - 300px - var(--su-static24))';
	} else {
		mainDiv.style.width = '100%';
	}
}());


+---------------------------------------+
| // Get image as Base64 encoded uri  	|
+---------------------------------------+
javascript: (function () {
	var newImg = document.getElementsByTagName('img')[0];
	var canvas = document.createElement('canvas'),
		ctx = canvas.getContext('2d');
	canvas.width = newImg.naturalWidth;
	canvas.height = newImg.naturalHeight;
	ctx.drawImage(newImg, 0, 0);
	var css = '.black_overlay{ display: block; position: absolute; top: 0%; left: 0%; width: 100%; height: 100%; background-color: black; z-index:1001; -moz-opacity: 0.8; opacity:.80; filter: alpha(opacity=80); }' + ' .white_content { display: block; position: absolute; top: 25%; left: 25%; width: 50%; height: 50%; padding: 16px; border: 16px solid orange; background-color: white; z-index:1002; word-wrap: break-word; overflow-y: scroll; }',
		head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');
	style.type = 'text/css';

	if (style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		style.appendChild(document.createTextNode(css));
	}

	head.appendChild(style);
	var baseHolder = document.createElement('textarea');
	baseHolder.id = 'light';
	baseHolder.setAttribute('class', 'white_content');
	baseHolder.value = canvas.toDataURL('image/png');
	var overlay = document.createElement('div');
	overlay.id = 'fade';
	overlay.setAttribute('class', 'black_overlay');
	overlay.onclick = function () {
		overlay.setAttribute('style', 'display: none');
		baseHolder.setAttribute('style', 'display: none');
	};

	document.body.appendChild(overlay);
	document.body.appendChild(baseHolder);
	console.log(canvas.toDataURL('image/png'));
}());