package DataStructuresAlgorithms.FinalTasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ArmyLunch {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int numberOfSoldiers = Integer.parseInt(reader.readLine());
        String[] array = reader.readLine().split(" ");
        List<String> list = new ArrayList<>();

        searchForSergant(array, list);
        searchForCorporals(array, list);
        searchForPrivates(array, list);

        for (String arr : list) {
            System.out.printf("%s ", arr);
        }
    }

    private static void searchForSergant(String[] array, List<String> list) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].startsWith("S")) {
                list.add(array[i]);
            }
        }
    }

    private static void searchForCorporals(String[] array, List<String> list) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].startsWith("C")) {
                list.add(array[i]);
            }
        }
    }

    private static void searchForPrivates(String[] array, List<String> list) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].startsWith("P")) {
                list.add(array[i]);
            }
        }
    }
}
