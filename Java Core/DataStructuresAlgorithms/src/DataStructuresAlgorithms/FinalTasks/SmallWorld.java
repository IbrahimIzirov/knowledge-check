package DataStructuresAlgorithms.FinalTasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SmallWorld {

    public static void main(String[] args) throws IOException {

        Scanner scan = new Scanner(System.in);

        String[] input = scan.nextLine().split(" ");

        int row = Integer.parseInt(input[0]);
        int col = Integer.parseInt(input[1]);

        int[][] table = new int[row][col];
        boolean[][] visited = new boolean[row][col];

        List<Integer> output = new ArrayList<>();
        int current = 0;

        for (int i = 0; i < row; i++) {
            String[] inputRow = scan.nextLine().split("");
            for (int j = 0; j < col; j++) {
                table[i][j] = Integer.parseInt(inputRow[j]);
            }
        }

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (table[i][j] == 0) continue;
                if (visited[i][j]) continue;
                current = (solveArea(table, visited, i, j, table[i][j]));
                output.add(current);
            }
        }

        Collections.sort(output);

        for (int i = output.size() - 1; i >= 0; i--) {
            System.out.println(output.get(i));
        }
    }

    private static int solveArea(int[][] table, boolean[][] visited, int row, int col, int target) {
        if (outOfTable(table, row, col)) return 0;
        if (visited[row][col]) return 0;
        if (table[row][col] != target) return 0;

        visited[row][col] = true;

        return 1 + solveArea(table, visited, row, col + 1, target)
                + solveArea(table, visited, row, col - 1, target)
                + solveArea(table, visited, row - 1, col, target)
                + solveArea(table, visited, row + 1, col, target);
    }

    private static boolean outOfTable(int[][] table, int row, int col) {
        return row < 0 || col < 0 || row >= table.length || col >= table[row].length;
    }
}
