package DataStructuresAlgorithms.FinalTasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DoctorsOffice {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> list = new ArrayList<>();

        String[] input = reader.readLine().split(" ");

        Map<String, Integer> map = new HashMap<>();

        while (!(input[0].equals("End"))) {
            if (input[0].equals("Append")) {
                list.add(input[1]);
                map.put(input[1], map.getOrDefault(input[1], 0) + 1);
                System.out.println("OK");
            }
            if (input[0].equals("Find")) {
                if (!map.containsKey(input[1])) {
                    System.out.println("0");
                } else {
                    System.out.println(map.get(input[1]));
                }
            }
            if (input[0].equals("Examine")) {
                int number = Integer.parseInt(input[1]);

                if (number > list.size()) {
                    System.out.println("Error");
                } else {

                    for (int i = 0; i < number; i++) {
                        String name = list.get(0);
                        list.remove(0);
                        int count = map.get(name);
                        map.put(name, count - 1);
                        System.out.print(String.format("%s ", name));
                    }

                    System.out.println();
                }
            }

            if (input[0].equals("Insert")) {
                int position = Integer.parseInt(input[1]);

                if (position > list.size()) {
                    System.out.println("Error");
                    input = reader.readLine().split(" ");
                    continue;
                } else {
                    if (position == list.size()) {
                        list.add(input[2]);
                    } else {
                        list.add(position, input[2]);
                    }

                    System.out.println("OK");
                    map.put(input[2], map.getOrDefault(input[2], 0) + 1);
                }
            }
            input = reader.readLine().split(" ");
        }
    }
}