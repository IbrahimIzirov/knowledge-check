package DataStructuresAlgorithms.FinalTasks;

import java.util.Scanner;

public class Sequence {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        String[] numbers = scan.nextLine().split(" ");

        int firstElement = Integer.parseInt(numbers[0]);
        int secondElement = Integer.parseInt(numbers[1]);

        System.out.println(power(firstElement, secondElement));
    }

    private static int power(int firstElement, int secondElement) {
        if (secondElement <= 1) {
            return firstElement;
        }

        switch (secondElement % 3) {
            case 0: {
                return 2 * power(firstElement, secondElement / 3) + 1;
            }
            case 1: {
                return power(firstElement, secondElement / 3) + 2;
            }
            case 2: {
                return power(firstElement, secondElement / 3 + 1) + 1;
            }
        }
        return firstElement;
    }
}
