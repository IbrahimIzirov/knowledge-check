package DataStructuresAlgorithms.LinearDataStructures;

public class ReserveLinkedList {
    public static void main(String[] args) {

    }

    public static ListNode reverseList(ListNode head) {
        ListNode result = null;
        ListNode next = null;

        //"1 ->2 ->3 ->4 ->5 ->NULL"
        while (head != null) {
            next = head.next; // next = 2
            head.next = result; // head.next = null
            result = head; // result = 1
            head = next; // 2
        }
        return result.next;
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
