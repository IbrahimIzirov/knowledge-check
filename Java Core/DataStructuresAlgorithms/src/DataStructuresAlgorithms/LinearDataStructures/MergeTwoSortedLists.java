package DataStructuresAlgorithms.LinearDataStructures;

public class MergeTwoSortedLists {
    public static void main(String[] args) {

    }

    public ListNode mergeTwoList(ListNode left, ListNode right) {
        ListNode head = new ListNode();
        ListNode result = head;

        while (left != null || right != null) {
            if (left != null && right != null) {
                if (left.val >= right.val) {
                    result.next = left;
                    left = left.next;
                } else {
                    result.next = right;
                    right = right.next;
                }
            } else {
                if (left != null) {
                    result.next = left;
                    left = left.next;
                }
                if (right != null) {
                    result.next = right;
                    right = right.next;
                }
            }
            result = result.next;
        }
        return head.next;
    }


    private class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
