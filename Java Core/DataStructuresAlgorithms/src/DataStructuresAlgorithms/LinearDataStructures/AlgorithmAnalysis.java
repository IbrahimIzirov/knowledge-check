package DataStructuresAlgorithms.LinearDataStructures;

public class AlgorithmAnalysis {
    public static void main(String[] args) {

        System.out.println(sumNM2(10, 5));
    }

    // Task 1 - Linear(On)
    static int product(int a, int b) {
        int sum = 0;
        for (int i = 0; i < b; i++) {
            sum += a;
        }
        return sum;
    }

    //Task 2 - Linear(On)
    static int power(int a, int b) {
        if (b < 0) {
            return 0;
        }
        if (b == 0) {
            return 1;
        }
        int power = a;
        while (b > 1) {
            power *= a;
            b--;
        }
        return power;
    }

    // Task 3 - Constant(1)
    static int mod(int a, int b) {
        if (b < 0) {
            return -1;
        }
        int div = a / b;
        return a - div * b;
    }

    //  Task 4 - Cubic(On3)
    static int sum3(int n) {
        int sum = 0;
        for (int a = 0; a < n; a++) {
            for (int b = 0; b < n; b++) {
                for (int c = 0; c < n; c++) {
                    sum += (a * b * c);
                }
            }
        }
        return sum;
    }

    // Task 5 - Quadratic(Onm)
    static int sumNM(int n, int m) {
        int sum = 0;
        for (int a = 0; a < n; a++) {
            for (int b = 0; b < m; b++) {
                sum += (a * b);
            }
        }
        return sum;
    }

    // Task 6 - Quadratic(On2)
    static int sumNM2(int n, int m) {
        int sum = 0;
        for (int a = 0; a < n; a++) {
            for (int b = 0; b < m; b++) {
                if (a == b) {
                    for (int c = 0; c < n; c++) {
                        sum += (a * b * c);
                    }
                }
            }
        }
        return sum;
    }

    // Task 7 - Linear O(n)
    static int factorial(int n) {
        int factorial = 1;
        while (n > 1) {
            factorial *= n;
            n--;
        }
        return factorial;
    }
}
