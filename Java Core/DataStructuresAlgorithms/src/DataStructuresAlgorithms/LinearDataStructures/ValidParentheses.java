package DataStructuresAlgorithms.LinearDataStructures;

import java.util.Scanner;
import java.util.Stack;

public class ValidParentheses {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();

        if (isValid(input)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

    public static boolean isValid(String s) {
        char[] charactersArray = s.toCharArray();
        if (s.length() == 0) return true;

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            if (charactersArray[i] == '(' || charactersArray[i] == '{' || charactersArray[i] == '[') {
                stack.push(charactersArray[i]);
            }
            if (charactersArray[i] == ')' || charactersArray[i] == '}' || charactersArray[i] == ']') {
                if (stack.isEmpty()) return false;
                char temp = stack.pop();
                if ((temp == '(' && charactersArray[i] == ')') || (temp == '{' && charactersArray[i] == '}') || (temp == '[' && charactersArray[i] == ']')) {
                    continue;
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
