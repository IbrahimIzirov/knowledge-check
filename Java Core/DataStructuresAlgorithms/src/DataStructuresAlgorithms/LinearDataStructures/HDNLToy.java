package DataStructuresAlgorithms.LinearDataStructures;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class HDNLToy {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int number = Integer.parseInt(scan.nextLine());

        List<String> list = new ArrayList<>();
        List<String> output = new ArrayList<>();
        Stack<String> holder = new Stack<>();
        int counter = 0;

        for (int i = 0; i < number; i++) {
            list.add(scan.nextLine());
        }

        for (int i = 1; i < number + 1; i++) {

            int firstElement = Integer.parseInt((list.get(i - 1).substring(1)));

            if (i == list.size()) {
                output.add(spaceHolder(counter, openBrackets(list.get(i - 1))));
                output.add(spaceHolder(counter, closeBrackets(list.get(i - 1))));
                break;

            } else {

                int secondElement = Integer.parseInt((list.get(i).substring(1)));

                if (firstElement < secondElement) {
                    output.add(spaceHolder(counter, openBrackets(list.get(i - 1))));
                    holder.push(spaceHolder(counter, closeBrackets(list.get(i - 1))));

                    counter++;
                    continue;
                }
                if (firstElement == secondElement) {
                    output.add(spaceHolder(counter, openBrackets(list.get(i - 1))));
                    output.add(spaceHolder(counter, closeBrackets(list.get(i - 1))));
                    continue;

                } else {
                    output.add(spaceHolder(counter, openBrackets(list.get(i - 1))));
                    holder.push(spaceHolder(counter, closeBrackets(list.get(i - 1))));

                    for (int j = 0; j <= holder.size(); j++) {

                        int firstIndex = holder.peek().indexOf("<");
                        int secondIndex = holder.peek().indexOf(">");
                        int element = Integer.parseInt(holder.peek().substring(firstIndex + 3, secondIndex));

                        if (element >= secondElement) {
                            output.add(holder.pop());
                            j = 0;
                        }
                    }
                    counter--;
                }
            }
        }
        for (int i = holder.size() - 1; i >= 0; i--) {
            output.add(holder.get(i));
        }

        System.out.println(String.join(System.lineSeparator(), output));
    }

    private static String openBrackets(String element) {
        StringBuilder builder = new StringBuilder();
        builder.append("<").append(element).append(">");
        return builder.toString();
    }

    private static String closeBrackets(String element) {
        StringBuilder builder = new StringBuilder();
        builder.append("</").append(element).append(">");
        return builder.toString();
    }

    private static String spaceHolder(int space, String element) {
        StringBuilder builder = new StringBuilder();
        String string = " ";

        for (int i = 0; i < space; i++) {
            if (space == 0) {
                break;
            }
            builder.append(string);
        }
        builder.append(element);

        return builder.toString();
    }
}