package DataStructuresAlgorithms.LinearDataStructures;

public class MiddleOfTheLinkedList {
    public static void main(String[] args) {

    }

    public ListNode middleNode(ListNode head) {
        //Имаме два указателя slow и fast, slow - ще го движим с next.
        //fast - ще го местим с 2 места напред.

        ListNode slow = head;
        ListNode fast = head;

        while (fast != null || fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    private class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
