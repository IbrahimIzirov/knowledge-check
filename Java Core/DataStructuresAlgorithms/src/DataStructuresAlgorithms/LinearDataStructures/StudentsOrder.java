package DataStructuresAlgorithms.LinearDataStructures;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentsOrder {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int studentsCount = Integer.parseInt(scan.next());
        int seatsCount = Integer.parseInt(scan.next());
        List<String> persons = new ArrayList<>();

        while (studentsCount > 0) {
            persons.add(scan.next());
            studentsCount--;
        }

        while (seatsCount > 0) {
            String leftPerson = scan.next();
            String rightPerson = scan.next();

            persons.remove(leftPerson);

            int index = persons.indexOf(rightPerson);

            persons.add(index, leftPerson);

            seatsCount--;
        }

        System.out.println(String.join(" ", persons));
    }
}
