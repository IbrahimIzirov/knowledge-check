package DataStructuresAlgorithms.LinearDataStructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Stack;

public class Jumps {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int inputNumber = Integer.parseInt(reader.readLine());
        String[] input = reader.readLine().split(" ");
        int[] array = new int[inputNumber];

        for (int i = 0; i < inputNumber; i++) {
            array[i] = Integer.parseInt(input[i]);
        }

        int head = 0;
        int biggest = 0;
        int count = -1;

        for (int i = 0; i < inputNumber; i++) {
            for (int j = i; j < inputNumber; j++) {
                int element = array[j];
                if (head < element) {
                    head = element;
                    count++;
                }
            }

            if (count > biggest) {
                biggest = count;
            }
            array[i] = count;
            count = -1;
            head = 0;
        }

        System.out.println(biggest);
        System.out.println(Arrays.toString(array)
                .replaceAll(", ", " ")
                .replace("[", "")
                .replace("]", ""));

    }

    public static void correctTask() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        int[] nums = Arrays
                .stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        int max = 0;
        int[] result = new int[n];

        Stack<Integer> indexes = new Stack<>();

        for (int i = n - 1; i >= 0; i--) {
            while (!indexes.isEmpty() && nums[indexes.peek()] <= nums[i]) {
                result[indexes.pop()] = indexes.size();
            }

            indexes.push(i);
            result[i] = indexes.size() - 1;
            max = Math.max(result[i], max);
        }
        System.out.println(max);
        System.out.println(Arrays.toString(result)
                .replace("[", "")
                .replace("]", "")
                .replace(",", ""));
    }
}