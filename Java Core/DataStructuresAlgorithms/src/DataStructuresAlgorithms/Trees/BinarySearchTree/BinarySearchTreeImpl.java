package DataStructuresAlgorithms.Trees.BinarySearchTree;

import java.util.*;

public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {
    private E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;

    public BinarySearchTreeImpl(E value) {
        this.value = value;
        left = null;
        right = null;
    }

    @Override
    public E getRootValue() {
        return value;
    }

    @Override
    public BinarySearchTree<E> getLeftTree() {
        return left;
    }

    @Override
    public BinarySearchTree<E> getRightTree() {
        return right;
    }

    @Override
    public void insert(E value) {

        if (this.value.compareTo(value) > 0) {
            if (this.left == null) {
                left = new BinarySearchTreeImpl<>(value);
            } else {
                left.insert(value);
            }
        } else if (this.value.compareTo(value) < 0) {
            if (this.right == null) {
                right = new BinarySearchTreeImpl<>(value);
            } else {
                right.insert(value);
            }
        }
    }


    @Override
    public boolean search(E value) {

        Queue<BinarySearchTreeImpl<E>> queue = new LinkedList<>();

        queue.offer(this);

        while (!queue.isEmpty()) {

            BinarySearchTreeImpl<E> current = queue.poll();

            if (current.value.equals(value)) return true;

            if (current.left != null) {
                queue.offer(current.left);
            }
            if (current.right != null) {
                queue.offer(current.right);
            }
        }

        return false;
    }

    @Override
    public List<E> inOrder() {
        List<E> list = new ArrayList<>();

        if (value == null) {
            return list;
        }

        if (left != null) {
            list.addAll(left.inOrder());
        }

        list.add(value);

        if (right != null) {
            list.addAll(right.inOrder());
        }
        return list;
    }


    @Override
    public List<E> postOrder() {
        List<E> list = new ArrayList<>();

        if (value == null) {
            return list;

        } else {

            if (left != null) {
                list.addAll(left.postOrder());
            }

            if (right != null) {
                list.addAll(right.postOrder());
            }

            list.add(value);

            return list;
        }
    }


    @Override
    public List<E> preOrder() {
        List<E> list = new ArrayList<>();

        list.add(value);

        if (value == null) {
            return list;

        } else {

            if (left != null) {
                list.addAll(left.preOrder());
            }

            if (right != null) {
                list.addAll(right.preOrder());
            }

            return list;
        }
    }

    @Override
    public List<E> bfs() {
        List<E> list = new ArrayList<>();

        Queue<BinarySearchTreeImpl<E>> queue = new LinkedList<>();

        queue.offer(this);

        while (!queue.isEmpty()) {
            BinarySearchTreeImpl<E> current = queue.poll();

            list.add(current.value);

            if (current.left != null) {
                queue.offer(current.left);
            }
            if (current.right != null) {
                queue.offer(current.right);
            }
        }
        return list;
    }

    @Override
    public int height() {

        if (this.left == null && this.right == null) {
            return 0;
        } else {
            int counter = 0;
            if (left != null) {
                counter = Math.max(counter, left.height());
            }
            if (right != null) {
                counter = Math.max(counter, right.height());
            }
            return counter + 1;
        }
    }


// Advanced task: implement remove method. To test, uncomment the commented tests in BinaryTreeImplTests
//    @Override
//    public boolean remove(E value) {
//        throw new UnsupportedOperationException();
//    }
}
