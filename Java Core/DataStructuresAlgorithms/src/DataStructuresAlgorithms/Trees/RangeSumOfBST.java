package DataStructuresAlgorithms.Trees;

import java.util.Stack;

public class RangeSumOfBST {
    public static void main(String[] args) {
                /*
        the tree:
           10
          / \
         5   15
        / \   \
       3   7   18

         */

        // Given the root node of a binary search tree,
        // return the sum of values of all nodes with a value in the range [low, high].

        // Input: root = [10,5,15,3,7,null,18], low = 7, high = 15
        // Output: 32

        TreeNode<Integer> root = new TreeNode<>(10);

        TreeNode<Integer> leftNode = new TreeNode<>(5,
                new TreeNode<>(3),
                new TreeNode<>(7));

        TreeNode<Integer> rightNode = new TreeNode<>(15,
                null,
                new TreeNode<>(18));

        root.left = leftNode;
        root.right = rightNode;

        System.out.printf("Recursion: %d%n", rangeSumBSTRec(root, 7, 15));

        System.out.printf("Iterative: %d", rangeSumBSTIterative(root, 7, 15));
    }

    public static int rangeSumBSTRec(TreeNode<Integer> root, int low, int high) {

        // base case
        if (root == null)
            return 0;

        if (root.val > high)
            return rangeSumBSTRec(root.left, low, high);

        else if (root.val < low)
            return rangeSumBSTRec(root.right, low, high);

        return root.val + rangeSumBSTRec(root.left, low, high) + rangeSumBSTRec(root.right, low, high);
    }

    public static int rangeSumBSTIterative(TreeNode<Integer> root, int low, int high) {
        Stack<TreeNode<Integer>> stk = new Stack<>();
        stk.push(root);
        int sum = 0;
        while (!stk.isEmpty()) {
            TreeNode<Integer> current = stk.pop();
            if (current == null) continue;
            if (current.val > low) stk.push(current.left);  // left child is a possible candidate.
            if (current.val < high) stk.push(current.right);  // right child is a possible candidate.
            if (low <= current.val && current.val <= high) sum += current.val;
        }
        return sum;
    }


    public static class TreeNode<T> {
        T val;
        TreeNode<T> left;
        TreeNode<T> right;

        TreeNode() {

        }

        TreeNode(T val) {
            this.val = val;
        }

        TreeNode(T val, TreeNode<T> left, TreeNode<T> right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
