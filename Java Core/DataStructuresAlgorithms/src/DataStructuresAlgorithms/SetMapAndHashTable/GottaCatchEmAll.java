package DataStructuresAlgorithms.SetMapAndHashTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class GottaCatchEmAll {
    private static class Item implements Comparable<Item> {
        String name;
        String pokemonType;
        int power;
        int position;

        public Item(String name, String pokemonType, int power, int position) {
            this.name = name;
            this.pokemonType = pokemonType;
            this.power = power;
            this.position = position;
        }

        public String getName() {
            return name;
        }

        public int getPower() {
            return power;
        }

        @Override
        public int compareTo(Item o) {
            if (this.getName().compareTo(o.getName()) > 0) {
                return 1;
            }
            if (this.getName().compareTo(o.getName()) < 0) {
                return -1;
            }
            if (this.getPower() > o.getPower()) {
                return -1;
            }
            if (this.getPower() < o.getPower()) {
                return 1;
            }
            return 0;
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", getName(), getPower());
        }
    }

    private static final List<Item> pokemons = new ArrayList<>();
    private static final Map<String, TreeSet<Item>> pokemonsSorted = new HashMap<>();
    public static StringBuilder builder = new StringBuilder();

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] command = reader.readLine().split(" ");

        while (!"end".equals(command[0])) {

            if (command[0].equals("add")) {
                addItem(command[1], command[2], Integer.parseInt(command[3]), Integer.parseInt(command[4]));
            }

            if (command[0].equals("find")) {
                String commandType = command[1];

                printFind(commandType);
            }

            if (command[0].equals("ranklist")) {

                int start = Integer.parseInt(command[1]);
                int end = Integer.parseInt(command[2]);

                printRankList(start, end);
            }
            command = reader.readLine().split(" ");
        }
        System.out.println(builder.toString());
    }

    private static void printFind(String type) {

        if (pokemonsSorted.containsKey(type)) {
            StringBuilder resultPokemons = new StringBuilder();
            int count = 1;
            for (Item pokemon : pokemonsSorted.get(type)) {
                if (count > 5) {
                    break;
                }
                resultPokemons.append(pokemon.toString()).append("; ");
                count++;
            }
            builder.append(String.format("Type %s: %s%n", type, resultPokemons.substring(0, resultPokemons.length() - 2)));
        } else {
            builder.append(String.format("Type %s: %n", type));
        }
    }

    private static void addItem(String name, String type, int power, int position) {

        Item p = new Item(name, type, power, position);

        pokemons.add(position - 1, p);

        if (!pokemonsSorted.containsKey(type)) {
            pokemonsSorted.put(type, new TreeSet<>());
        }
        pokemonsSorted.get(type).add(p);

        builder.append(String.format("Added pokemon %s to position %d%n", name, position));
    }

    private static void printRankList(int firstIndex, int secondIndex) {

        for (int i = firstIndex - 1; i < secondIndex - 1; i++) {
            builder.append(String.format("%d. %s; ", i + 1, pokemons.get(i)));
        }

        builder.append(String.format("%d. %s", secondIndex, pokemons.get(secondIndex - 1)));

        builder.append(System.lineSeparator());
    }
}
