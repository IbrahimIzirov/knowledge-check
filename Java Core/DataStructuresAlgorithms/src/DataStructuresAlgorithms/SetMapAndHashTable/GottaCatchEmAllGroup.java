package DataStructuresAlgorithms.SetMapAndHashTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class GottaCatchEmAllGroup {
    private static class Pokemon implements Comparable<Pokemon> {
        String name;
        String type;
        int power;


        public Pokemon(String name, String type, int power) {
            this.name = name;
            this.type = type;
            this.power = power;
        }


        public String getName() {
            return name;
        }


        public int getPower() {
            return power;
        }


        @Override
        public int compareTo(Pokemon o) {
            return Comparator.comparing(Pokemon::getName)
                    .thenComparing(Comparator.comparing(Pokemon::getPower).reversed())
                    .compare(this, o);
        }


        @Override
        public String toString() {
            return String.format("%s(%d)", name, power);
        }
    }


    static Map<String, TreeSet<Pokemon>> pokemonMap = new HashMap<>();
    static List<Pokemon> pokemonList = new ArrayList<>();


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String[] splitCommand = bufferedReader.readLine().split(" ");


        for (int i = 0; i < 100000; i++) {
            switch (splitCommand[0].toLowerCase()) {
                case "add":
                    addPokemon(splitCommand[1],
                            splitCommand[2],
                            Integer.parseInt(splitCommand[3]),
                            Integer.parseInt(splitCommand[4]));
                    break;
                case "find":
                    findByType(splitCommand[1]);
                    break;
                case "ranklist":
                    rankPokemons(splitCommand[1], splitCommand[2]);
                    break;
                case "end":
                    System.exit(0);
            }
            splitCommand = bufferedReader.readLine().split(" ");
        }
    }


    private static void rankPokemons(String rankStart, String rankEnd) {
        int intRankStart = Integer.parseInt(rankStart) - 1;
        int intRankEnd = Integer.parseInt(rankEnd);
        int count = Integer.parseInt(rankStart);
        StringBuilder result = new StringBuilder();
        for (int i = intRankStart; i < intRankEnd; i++) {
            result.append(String.format("%d. %s; ", count++, pokemonList.get(i)));
        }
        System.out.println(result.substring(0, result.length() - 2));
    }


    private static void findByType(String type) {
        if (pokemonMap.containsKey(type)) {
            StringBuilder resultPokemons = new StringBuilder();
            int count = 1;
            for (Pokemon pokemon : pokemonMap.get(type)) {
                if (count > 5) {
                    break;
                }
                resultPokemons.append(pokemon.toString()).append("; ");
                count++;
            }
            System.out.printf("Type %s: %s%n", type, resultPokemons.substring(0, resultPokemons.length() - 2));
        } else {
            System.out.printf("Type %s: %n", type);
        }
    }

    private static void addPokemon(String name, String type, int power, int position) {
        Pokemon pokemon = new Pokemon(name, type, power);
        if (!pokemonMap.containsKey(type)) {
            pokemonMap.put(type, new TreeSet<>());
        }
        pokemonMap.get(type).add(pokemon);
        pokemonList.add(position - 1, pokemon);
        System.out.printf("Added pokemon %s to position %d%n", name, position);
    }
}
