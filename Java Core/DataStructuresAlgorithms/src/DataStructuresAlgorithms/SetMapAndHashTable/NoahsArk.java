package DataStructuresAlgorithms.SetMapAndHashTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class NoahsArk {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Integer> animals = new TreeMap<>();
        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {
            String animal = reader.readLine();
            if (animals.containsKey(animal)) {
                int oldCount = animals.get(animal);
                animals.replace(animal, oldCount + 1);
            } else {
                animals.put(animal, 1);
            }
//            animals.put(animal, animals.getOrDefault(animal, 0) + 1);
        }
        for (Map.Entry<String, Integer> entry : animals.entrySet()) {
            System.out.printf("%s %d %s%n",
                    entry.getKey(),
                    entry.getValue(),
                    entry.getValue() % 2 == 0 ? "Yes" : "No");
        }
    }
}
