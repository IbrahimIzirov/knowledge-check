package DataStructuresAlgorithms.SetMapAndHashTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class InvertoryManagerSecandTry {

    private static class Item implements Comparable<Item> {
        String name;
        Double price;
        String itemType;

        public Item(String name, Double price, String itemType) {
            setName(name);
            setPrice(price);
            setItemType(itemType);
        }

        public String getName() {
            return name;
        }

        public Double getPrice() {
            return price;
        }

        public String getItemType() {
            return itemType;
        }

        public void setName(String name) {
            if (name.length() < 3 || name.length() > 20) {
                throw new IllegalArgumentException("Error Exception");
            }
            this.name = name;
        }

        public void setPrice(Double price) {
            if (price < 0 || price > 5000) {
                throw new IllegalArgumentException("Error Exception");
            }
            this.price = price;
        }

        public void setItemType(String itemType) {
            if (name.length() < 3 || name.length() > 20) {
                throw new IllegalArgumentException("Error Exception");
            }
            this.itemType = itemType;
        }

        @Override
        public String toString() {
            return String.format("%s(%s)", getName(), fmt(getPrice()));
        }

        @Override
        public int compareTo(Item o) {
            return Comparator.comparingDouble(Item::getPrice)
                    .thenComparing(Item::getName)
                    .thenComparing(Item::getItemType)
                    .compare(this, o);
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map<String, Item> inventory = new TreeMap<>();
        StringBuilder stringBuilder = new StringBuilder();

        String[] command = reader.readLine().split(" ");
        while (!"end".equals(command[0])) {
            try {
                if (command[0].equals("add")) {
                    if (!inventory.containsKey(command[1])) {
                        inventory.put(command[1], add(command[1], Double.parseDouble(command[2]), command[3]));
                        stringBuilder.append(String.format("Ok: Item %s added successfully%n", command[1]));
                    } else {
                        stringBuilder.append(String.format("Error: Item %s already exists%n", command[1]));
                        command = reader.readLine().split(" ");
                        continue;
                    }
                }
            } catch (IllegalArgumentException e) {
                stringBuilder.append("Error");
            }

            if (command[0].equals("filter") && command[2].equals("type")) {
                String givenType1 = command[3];
                boolean isTypeValid = inventory.values().stream().anyMatch(item -> item.getItemType().equals(givenType1));
                if (!isTypeValid) {
                    stringBuilder.append(String.format("Error: Type %s does not exists%n", givenType1));
                } else {
                    String givenType = command[3];
                    String filterAndSort = inventory.values()
                            .stream()
                            .filter(item -> item.getItemType().equals(givenType))
                            .sorted()
                            .map(Item::toString)
                            .limit(10)
                            .collect(Collectors.joining(", "));
                    stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                    command = reader.readLine().split(" ");
                    continue;
                }
            }

            if (command[0].equals("filter") && command[2].equals("price")) {
                if (command.length == 7) {
                    double minPrice = Double.parseDouble(command[4]);
                    double maxPrice = Double.parseDouble(command[6]);
                    if (minPrice >= 0 && maxPrice >= 0) {
                        String filterAndSort = inventory.values()
                                .stream()
                                .filter(item -> minPrice <= item.getPrice() && item.getPrice() <= maxPrice)
                                .sorted()
                                .map(Item::toString)
                                .limit(10)
                                .collect(Collectors.joining(", "));
                        stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                        command = reader.readLine().split(" ");
                        continue;
                    }
                }

                if (command.length == 5) {
                    if (command[3].equals("from")) {
                        double minPrice = Double.parseDouble(command[4]);
                        String filterAndSort = inventory.values()
                                .stream()
                                .filter(item -> minPrice <= item.getPrice())
                                .sorted()
                                .map(Item::toString)
                                .limit(10)
                                .collect(Collectors.joining(", "));
                        stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                        command = reader.readLine().split(" ");
                        continue;
                    }
                    if (command[3].equals("to")) {
                        double maxPrice = Double.parseDouble(command[4]);
                        String filterAndSort = inventory.values()
                                .stream()
                                .filter(item -> item.getPrice() <= maxPrice)
                                .sorted()
                                .map(Item::toString)
                                .limit(10)
                                .collect(Collectors.joining(", "));
                        stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                        command = reader.readLine().split(" ");
                        continue;
                    }

                }
            }
            command = reader.readLine().split(" ");
        }
        System.out.println(stringBuilder);

    }

    static Item add(String name, Double price, String type) {
        return new Item(name, price, type);
    }

    static String fmt(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }
}