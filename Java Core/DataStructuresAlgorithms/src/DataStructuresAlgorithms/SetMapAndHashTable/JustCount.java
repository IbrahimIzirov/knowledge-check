package DataStructuresAlgorithms.SetMapAndHashTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class JustCount {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input = reader.readLine();

        Map<Character, Integer> lowerCase = new HashMap<>();
        Map<Character, Integer> upperCase = new HashMap<>();
        Map<Character, Integer> symbols = new HashMap<>();

        int lowerMaxCount = 0;
        int upperMaxCount = 0;
        int symbolMaxCount = 0;

        for (char ch : input.toCharArray()) {
            if (ch >= 65 && ch <= 90) {
                upperMaxCount = Math.max(upperMaxCount, addToMap(ch, upperCase));
            } else if (ch >= 97 && ch <= 122) {
                lowerMaxCount = Math.max(lowerMaxCount, addToMap(ch, lowerCase));
            } else if (ch >= 48 && ch <= 57) {
                continue;
            } else {
                symbolMaxCount = Math.max(symbolMaxCount, addToMap(ch, symbols));
            }
        }

        printResult(symbolMaxCount, symbols);
        printResult(lowerMaxCount, lowerCase);
        printResult(upperMaxCount, upperCase);

    }

    private static void printResult(int count, Map<Character, Integer> map) {
        if (count == 0) {
            System.out.println("-");
            return;
        }

        List<Character> result = new ArrayList<>();
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() == count) {
                result.add(entry.getKey());
            }
        }

        result.sort(Comparator.naturalOrder());
        System.out.printf("%s %d%n", result.get(0), count);
    }

    private static int addToMap(char ch, Map<Character, Integer> map) {
        map.put(ch, map.getOrDefault(ch, 0) + 1);
        return map.get(ch);
    }
}
