package DataStructuresAlgorithms.SetMapAndHashTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class InventoryManager {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Product> map = new HashMap<>();
        StringBuilder stringBuilder = new StringBuilder();

        String[] input = reader.readLine().split(" ");

        while (!(input[0].equals("end"))) {

            try {

                if (input[0].equals("add")) {
                    if (!map.containsKey(input[1])) {
                        map.put(input[1], add(input[1], Double.parseDouble(input[2]), input[3]));
                        stringBuilder.append(String.format("Ok: Item %s added successfully%n", input[1]));
                        input = reader.readLine().split(" ");
                        continue;
                    } else {
                        stringBuilder.append(String.format("Error: Item %s already exists%n", input[1]));
                        input = reader.readLine().split(" ");
                        continue;
                    }
                }
            } catch (IllegalArgumentException iaa) {
                System.out.println("Error");
            }

            if (input[0].equals("filter") && input[2].equals("type")) {
                String givenType1 = input[3];
                boolean isTypeValid = map.values().stream().anyMatch(item -> item.getType().equals(givenType1));
                if (!isTypeValid) {
                    stringBuilder.append(String.format("Error: Type %s does not exists%n", givenType1));
                } else {
                    String givenType = input[3];
                    String filterAndSort = map.values()
                            .stream()
                            .filter(item -> item.getType().equals(givenType))
                            .sorted()
                            .map(Product::toString)
                            .limit(10)
                            .collect(Collectors.joining(", "));
                    stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                    input = reader.readLine().split(" ");
                    continue;
                }
            }

            if (input[0].equals("filter") && input[2].equals("price")) {
                if (input.length == 7) {
                    double minPrice = Double.parseDouble(input[4]);
                    double maxPrice = Double.parseDouble(input[6]);
                    if (minPrice >= 0 && maxPrice >= 0) {
                        String filterAndSort = map.values()
                                .stream()
                                .filter(item -> minPrice <= item.getPrice() && item.getPrice() <= maxPrice)
                                .sorted()
                                .map(Product::toString)
                                .limit(10)
                                .collect(Collectors.joining(", "));
                        stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                        input = reader.readLine().split(" ");
                        continue;
                    }
                }

                if (input.length == 5) {
                    if (input[3].equals("from")) {
                        double minPrice = Double.parseDouble(input[4]);
                        String filterAndSort = map.values()
                                .stream()
                                .filter(item -> minPrice <= item.getPrice())
                                .sorted()
                                .map(Product::toString)
                                .limit(10)
                                .collect(Collectors.joining(", "));
                        stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                        input = reader.readLine().split(" ");
                        continue;
                    }

                    if (input[3].equals("to")) {
                        double maxPrice = Double.parseDouble(input[4]);
                        String filterAndSort = map.values()
                                .stream()
                                .filter(item -> item.getPrice() <= maxPrice)
                                .sorted()
                                .map(Product::toString)
                                .limit(10)
                                .collect(Collectors.joining(", "));
                        stringBuilder.append(String.format("Ok: %s%n", filterAndSort));
                        input = reader.readLine().split(" ");
                        continue;
                    }

                }
            }
            input = reader.readLine().split(" ");
        }
        System.out.println(stringBuilder);

    }

    static Product add(String name, Double price, String type) {
        return new Product(name, price, type);
    }

    private static class Product implements Comparable<Product> {

        String name;
        double price;
        String type;

        public Product(String name, double price, String type) {
            setName(name);
            setPrice(price);
            setType(type);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {

            if (name.length() < 3 || name.length() > 20) {
                throw new IllegalArgumentException("Error Exception");
            }
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            if (price < 0 || price > 5000) {
                throw new IllegalArgumentException("Error Exception");
            }
            this.price = price;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {

            if (name.length() < 3 || name.length() > 20) {
                throw new IllegalArgumentException("Error Exception");
            }
            this.type = type;
        }

        @Override
        public String toString() {
            return String.format("%s(%s)", getName(), getPrice());
        }

        public int compareTo(Product o) {
            return Comparator.comparingDouble(Product::getPrice)
                    .thenComparing(Product::getName)
                    .thenComparing(Product::getType)
                    .compare(this, o);
        }
    }
}