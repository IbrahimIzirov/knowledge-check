package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class ScroogeMcDuck {

    private static int counter = 0;
    private static int startPositionRow = 0;
    private static int startPositionCol = 0;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] input = scan.nextLine().split(" ");

        int row = Integer.parseInt(input[0]);
        int col = Integer.parseInt(input[1]);

        int[][] labyrinth = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                labyrinth[i][j] = scan.nextInt();
                if (labyrinth[i][j] == 0) {
                    startPositionRow = i;
                    startPositionCol = j;
                }
            }
        }

        solveLabyrinth(labyrinth, startPositionRow, startPositionCol);
    }

    private static void solveLabyrinth(int[][] labyrinth, int row, int col) {
        if (isOutOfLabyrinth(labyrinth, row, col)) {
            return;
        }

        if (labyrinth[row][col] > 0) {
            labyrinth[row][col] = labyrinth[row][col] - 1;
            counter++;
        }

        int leftNeighbor = getLeftNeighbor(labyrinth, row, col);
        int rightNeighbor = getRightNeighbor(labyrinth, row, col);
        int uppNeighbor = getUpNeighbor(labyrinth, row, col);
        int downNeighbor = getDownNeighbor(labyrinth, row, col);

        if (leftNeighbor == 0 &&
                rightNeighbor == 0 &&
                uppNeighbor == 0 &&
                downNeighbor == 0) {
            System.out.println(counter);
            return;
        }


        checkNeighboors(labyrinth, row, col,
                leftNeighbor, rightNeighbor,
                uppNeighbor, downNeighbor);

    }

    private static void checkNeighboors(int[][] labyrinth, int row, int col, int leftNeighbor, int rightNeighbor, int uppNeighbor, int downNeighbor) {
        if (leftNeighbor >= rightNeighbor &&
                leftNeighbor >= uppNeighbor &&
                leftNeighbor >= downNeighbor) {
            solveLabyrinth(labyrinth, row, col - 1);

        } else if (rightNeighbor >= uppNeighbor && rightNeighbor >= downNeighbor) {
            solveLabyrinth(labyrinth, row, col + 1);

        } else if (uppNeighbor >= downNeighbor) {
            solveLabyrinth(labyrinth, row - 1, col);

        } else {
            solveLabyrinth(labyrinth, row + 1, col);
        }
    }

    private static int getRightNeighbor(int[][] maze, int row, int col) {
        if (isOutOfLabyrinth(maze, row, col + 1)) {
            return 0;
        }
        return maze[row][col + 1];
    }

    private static int getLeftNeighbor(int[][] maze, int row, int col) {
        if (isOutOfLabyrinth(maze, row, col - 1)) {
            return 0;
        }
        return maze[row][col - 1];
    }

    private static int getUpNeighbor(int[][] maze, int row, int col) {
        if (isOutOfLabyrinth(maze, row - 1, col)) {
            return 0;
        }
        return maze[row - 1][col];
    }

    private static int getDownNeighbor(int[][] maze, int row, int col) {
        if (isOutOfLabyrinth(maze, row + 1, col)) {
            return 0;
        }
        return maze[row + 1][col];
    }

    private static boolean isOutOfLabyrinth(int[][] maze, int row, int col) {
        return row >= maze.length
                || row < 0
                || col >= maze[0].length
                || col < 0;
    }
}
