package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class CountOccurrences {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());

        System.out.println(counter(n));
    }

    private static int counter(int n) {

        if (n == 0) {
            return 0;
        }
        if (n % 10 == 7) {
            return 1 + counter(n / 10);
        }
        return counter(n / 10);
    }
}
