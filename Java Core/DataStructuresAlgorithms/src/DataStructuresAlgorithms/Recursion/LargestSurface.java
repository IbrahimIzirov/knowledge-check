package DataStructuresAlgorithms.Recursion;

import java.io.IOException;
import java.util.Scanner;

public class LargestSurface {

    private static int maxNeighborsCount = 0;
    private static int counter = 0;
    private static int[][] matrix;
    private static int[][] matrixVisited;

    public static void main(String[] args) throws IOException {

        Scanner scan = new Scanner(System.in);

        int row = scan.nextInt();
        int col = scan.nextInt();


        matrix = new int[row][col];
        matrixVisited = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = scan.nextInt();
            }
        }

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                counter = 0;
                solveArea(i, j);
                if (counter > maxNeighborsCount) {
                    maxNeighborsCount = counter;
                }
            }
        }
        System.out.println(maxNeighborsCount);
    }

    private static void solveArea(int row, int col) {
        if (isOutOfMatrix(row, col)) {
            return;
        }

        if (isVisited(row, col)) {
            return;
        }

        matrixVisited[row][col] = 1;
        counter++;

        if (!isOutOfMatrix(row, col + 1))
            if (matrix[row][col] == matrix[row][col + 1]) {
                solveArea(row, col + 1);
            }
        if (!isOutOfMatrix(row - 1, col))
            if (matrix[row][col] == matrix[row - 1][col]) {
                solveArea(row - 1, col);
            }
        if (!isOutOfMatrix(row + 1, col))
            if (matrix[row][col] == matrix[row + 1][col]) {
                solveArea(row + 1, col);
            }
        if (!isOutOfMatrix(row, col - 1))
            if (matrix[row][col] == matrix[row][col - 1]) {
                solveArea(row, col - 1);
            }
        return;
    }

    private static boolean isVisited(int row, int col) {
        if (matrixVisited[row][col] == 1) {
            return true;
        }
        return false;
    }

    private static boolean isOutOfMatrix(int row, int col) {
        return row >= matrix.length
                || row < 0
                || col >= matrix[0].length
                || col < 0;
    }
}