package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class BunnyEars2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = Integer.parseInt(scan.nextLine());

        System.out.println(bunnyEarsSum(number));
    }

    private static int bunnyEarsSum(int number) {

        if (number == 0) {
            return number;
        }

        if (number % 2 == 0) {
            return 3 + bunnyEarsSum(number - 1);
        } else {
            return 2 + bunnyEarsSum(number - 1);
        }
    }
}
