package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = Integer.parseInt(scan.nextLine());

        System.out.println(numberBlocks(number));
    }

    private static int numberBlocks(int number) {
        if (number == 0 || number == 1) {
            return number;
        }
        return number + numberBlocks(number - 1);
    }
}
