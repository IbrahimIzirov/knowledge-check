package DataStructuresAlgorithms.Recursion;

import java.util.Arrays;
import java.util.Scanner;

public class Variations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int variations = Integer.parseInt(scan.nextLine());

        String[] symbols = scan.nextLine().split(" ");

        Arrays.sort(symbols);

        printAllCharacters(symbols, variations);

    }

    private static void printAllCharacters(String[] array, int variations) {
        int n = array.length;

        SolveTheTask(array, "", n, variations);
    }

    private static void SolveTheTask(String[] array, String prefix, int n, int variations) {

        if (variations == 0) {
            System.out.println(prefix);
            return;
        }

        for (int i = 0; i < n; i++) {

            String newElement = prefix + array[i];

            SolveTheTask(array, newElement, n, variations - 1);
        }
    }
}
