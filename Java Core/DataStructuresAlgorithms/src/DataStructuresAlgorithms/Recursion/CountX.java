package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class CountX {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();

        System.out.println(numberOfChars(input));

    }

    private static int numberOfChars(String input) {
        if (input.length() == 0) {
            return 0;
        }

        if (input.charAt(input.length() - 1) == 'x') {
            return 1 + numberOfChars(input.substring(0, input.length() - 1));
        }
        return numberOfChars(input.substring(0, input.length() - 1));
    }
}
