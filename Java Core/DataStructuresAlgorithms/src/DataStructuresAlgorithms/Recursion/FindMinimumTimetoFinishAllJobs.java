package DataStructuresAlgorithms.Recursion;

import java.util.Arrays;

public class FindMinimumTimetoFinishAllJobs {

    private static int res = Integer.MAX_VALUE;

    public static void main(String[] args) {

        int[] arr = new int[]{12, 13, 14, 17, 25};
        int k = 3;

        System.out.println(minimumTimeRequired(arr, k));
    }

    public static int minimumTimeRequired(int[] jobs, int k) {

        Arrays.sort(jobs);
        dfs(jobs, jobs.length - 1, new int[k]);
        return res;
    }

    private static void dfs(int[] jobs, int pos, int[] sum) {
        if (pos < 0) {
            res = Math.min(res, Arrays.stream(sum).max().getAsInt());
            return;
        }
        if (Arrays.stream(sum).max().getAsInt() >= res) return;
        for (int i = 0; i < sum.length; i++) {
            if (i > 0 && sum[i] == sum[i - 1]) continue;
            sum[i] += jobs[pos];
            dfs(jobs, pos - 1, sum);
            sum[i] -= jobs[pos];
        }
    }
}
