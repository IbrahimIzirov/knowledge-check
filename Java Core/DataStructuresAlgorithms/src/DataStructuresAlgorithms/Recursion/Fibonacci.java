package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        long number = Integer.parseInt(scan.nextLine());

        System.out.println(fibonacciSum(number));

    }

    private static long fibonacciSum(long number) {
        if (number < 2) {
            return number;
        }
        return fibonacciSum(number - 1) + fibonacciSum(number - 2);
    }
}
