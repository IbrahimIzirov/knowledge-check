package DataStructuresAlgorithms.Recursion;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayWith6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] array = Arrays.stream(scan.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int index = Integer.parseInt(scan.nextLine());

        System.out.println(checkIfHas6(array, index));

    }

    private static boolean checkIfHas6(int[] array, int index) {
        if (index >= array.length) {
            return false;
        }

        if (array[index] == 6) {
            return true;
        }
        return checkIfHas6(array, index + 1);
    }

}
