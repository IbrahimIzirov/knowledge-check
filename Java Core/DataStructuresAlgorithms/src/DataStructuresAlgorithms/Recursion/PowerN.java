package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class PowerN {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = Integer.parseInt(scan.nextLine());
        int base = Integer.parseInt(scan.nextLine());

        System.out.println(getPower(number, base));
    }

    private static int getPower(int number, int base) {
        if (base == 0) {
            return 1;
        }

        return number * getPower(number, base - 1);

    }
}
