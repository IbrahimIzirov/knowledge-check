package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class BunnyEars {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = Integer.parseInt(scan.nextLine());

        System.out.println(bunnyEarsCount(number));
    }

    private static int bunnyEarsCount(int number) {
        if (number == 0) {
            return 0;
        }


        return 2 + bunnyEarsCount(number - 1);
    }
}
