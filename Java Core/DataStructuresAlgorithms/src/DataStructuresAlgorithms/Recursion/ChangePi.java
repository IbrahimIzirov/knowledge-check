package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class ChangePi {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();

        System.out.println(changePi(input));
    }

    private static String changePi(String input) {
        if (input.length() < 2) {
            return input;
        }
        if (input.startsWith("pi")) {
            return "3.14" + changePi(input.substring(2));
        }
        return input.charAt(0) + changePi(input.substring(1));
    }
}
