package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class SumDigits {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());

        System.out.println(sumDigitsRecursion(n));
    }

//    private static int sumDigits(String[] number) {
//        int sum = 0;
//        for (int i = 0; i < number.length; i++) {
//            int currentElement = Integer.parseInt(number[i]);
//            sum += currentElement;
//        }
//        return sum;
//    }

    private static int sumDigitsRecursion(int n) {
        if (n == 0) {
            return n;
        }

        return (n % 10 + sumDigitsRecursion(n / 10));
    }
}
