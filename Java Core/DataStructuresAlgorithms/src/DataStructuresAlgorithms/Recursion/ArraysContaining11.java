package DataStructuresAlgorithms.Recursion;

import java.util.Arrays;
import java.util.Scanner;

public class ArraysContaining11 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] array = Arrays.stream(scan.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int index = Integer.parseInt(scan.nextLine());

        System.out.println(checkIfcontaining11(array, index));
    }

    private static int checkIfcontaining11(int[] array, int index) {
        if (index >= array.length) {
            return 0;
        }

        if (array[index] == 11) {
            return 1 + checkIfcontaining11(array, index + 1);
        }
        return checkIfcontaining11(array, index + 1);
    }
}
