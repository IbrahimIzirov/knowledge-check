package DataStructuresAlgorithms.Recursion;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayValuesTimes10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] array = Arrays.stream(scan.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int index = Integer.parseInt(scan.nextLine());

        System.out.println(checkArray(array, index));
    }

    private static boolean checkArray(int[] array, int index) {
        if (index >= array.length - 1) {
            return false;
        }

        if (array[index] * 10 == array[index + 1]) {
            return true;
        }
        return checkArray(array, index + 1);
    }
}
