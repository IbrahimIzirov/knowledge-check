package DataStructuresAlgorithms.Recursion;

import java.util.*;

public class Cipher {
    public static void main(String[] args) {

        //Input
        //1122
        //A1B12C11D2

        //Output
        //3
        //AADD
        //ABD
        //CDD

        //Input
        //778
        //Z123A7787X666Y234

        //Output
        //0

        Scanner scan = new Scanner(System.in);

        String cipher = scan.nextLine();

        String message = scan.nextLine();

        String[] secretCode = message.split("[^A-Z0-9]+|(?<=[A-Z])(?=[0-9])|(?<=[0-9])(?=[A-Z])");

        int n = secretCode.length / 2;

        String[] letters = new String[n];

        String[] codes = new String[n];

        for (int i = 0; i < 2 * n; i += 2) {
            letters[i / 2] = secretCode[i];
            codes[i / 2] = secretCode[i + 1];
        }

        Map<String, String> map = new HashMap<>();

        for (int i = 0; i < n; i++) {
            map.put(codes[i], letters[i]);
        }

        Set<String> set = new TreeSet<>();

        SolveTheCipher(cipher, map, set, new StringBuilder());

        System.out.println(set.size());

        for (String elements : set) {
            System.out.println(elements);
        }

    }

    static void SolveTheCipher(String cipher, Map<String, String> map, Set<String> set, StringBuilder builder) {

        if (cipher.equals("")) {
            set.add(builder.toString());
            return;
        }

        for (String index : map.keySet()) {
            if (cipher.startsWith(index)) {
                String newMessage = cipher.substring(index.length());
                builder.append(map.get(index));

                SolveTheCipher(newMessage, map, set, builder);

                builder.setLength(builder.length() - 1);
            }
        }
    }
}
