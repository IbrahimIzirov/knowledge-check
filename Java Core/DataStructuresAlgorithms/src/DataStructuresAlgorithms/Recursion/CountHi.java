package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class CountHi {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();

        System.out.println(numberOfWordHi(input));

    }

    private static int numberOfWordHi(String input) {
        if (input.length() < 2) {
            return 0;
        }

        if (input.charAt(input.length() - 1) == 'i') {
            if (input.charAt(input.length() - 2) == 'h') {
                return 1 + numberOfWordHi(input.substring(0, input.length() - 1));
            }
        }
        return numberOfWordHi(input.substring(0, input.length() - 1));
    }
}
