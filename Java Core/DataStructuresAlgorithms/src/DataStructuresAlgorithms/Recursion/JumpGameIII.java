package DataStructuresAlgorithms.Recursion;

public class JumpGameIII {
    public static void main(String[] args) {

        int[] arr = new int[]{3, 0, 2, 1, 2};
        int start = 2;

        System.out.println(canReach(arr, start));
    }

    public static boolean canReach(int[] arr, int start) {

        if (start < 0 || start >= arr.length || arr[start] < 0) {
            return false;
        }

        arr[start] *= -1;
        return arr[start] == 0 || canReach(arr, start + arr[start]) || canReach(arr, start - arr[start]);
    }
}
