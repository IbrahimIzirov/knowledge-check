package DataStructuresAlgorithms.Recursion;

import java.util.Scanner;

public class CountOccurrences2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int number = Integer.parseInt(scan.nextLine());

        System.out.println(counter(number));
    }

    private static int counter(int number) {

        if (number == 0) {
            return 0;
        }
        if (number % 10 == 8) {
            if (number / 10 % 10 == 8) {
                return 2 + counter(number / 10);
            }
            return 1 + counter(number / 10);
        }
        return counter(number / 10);
    }
}
