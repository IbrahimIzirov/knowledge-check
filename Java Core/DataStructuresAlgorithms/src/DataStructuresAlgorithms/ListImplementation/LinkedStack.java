package DataStructuresAlgorithms.ListImplementation;

import DataStructuresAlgorithms.ListImplementation.contracts.Stack;

import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {

    private Node head;
    private int size;
    private E[] array;


    public LinkedStack() {
        this.head = null;
        this.size = 0;
    }

    public LinkedStack(int size) {
        this.head = null;
        this.size = 0;
    }

    @Override
    public void push(E element) {
        Node<E> temp = new Node<>(element);

        if (size > 0) {
            head.setPrev(element);
        }

        temp.setNext(head);
        head = temp;
        size++;
    }

    @Override
    public E pop() {
        if (head == null) {
            throw new NoSuchElementException();
        } else {
            Node temp = head;
            head = head.next;
            return (E) temp.data;
        }
    }

    @Override
    public E peek() {
        if (head == null) {
            throw new NoSuchElementException();
        } else {
            return (E) head.data;
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        Node<E> temp;
        for (temp = head; temp != null; temp = temp.next) {
            builder.append(temp.data.toString() + " ");
        }
        builder.append("]");
        return builder.toString();
    }

    private class Node<E> {
        public E prev;
        public E data;
        public Node<E> next;

        public Node(E data) {
            this.data = data;
        }

        public E getPrev() {
            return prev;
        }

        public void setPrev(E prev) {
            this.prev = prev;
        }

        public E getData() {
            return data;
        }

        public void setData(E data) {
            this.data = data;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }
}


