package DataStructuresAlgorithms.ListImplementation;

import DataStructuresAlgorithms.ListImplementation.contracts.Queue;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {

    private static final int INITIAL_SIZE_ARRAY = 10;

    private E[] data;
    private int head, tail, size;


    public ArrayQueue() {
        this(INITIAL_SIZE_ARRAY);
    }

    public ArrayQueue(int size) {
        this.data = (E[]) new Object[size];
        this.tail = 0;
    }

    @Override
    public void enqueue(E element) {
        if (data.length == size) {
            this.data = Arrays.copyOf(this.data, data.length * 2 + 1);
        }
        data[tail++] = element;
        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        E result = data[head];
        data[head] = null;
        head++;
        size--;
        return result;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return data[head];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}

