package DataStructuresAlgorithms.ListImplementation;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack<E> implements DataStructuresAlgorithms.ListImplementation.contracts.Stack<E> {

    private static final int INITIAL_SIZE_ARRAY = 4;
    private E[] data;
    private int top;
    private int size;

    public ArrayStack() {
        this(INITIAL_SIZE_ARRAY);
    }

    public ArrayStack(int size) {
        this.data = (E[]) new Object[size];
        top = 0;
        this.size = 0;
    }

    @Override
    public void push(E element) {
        if (data.length == size) {
            this.data = Arrays.copyOf(this.data, data.length * 2);
        }

        data[top++] = element;
        size++;
    }

    @Override
    public E pop() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        E result = data[top - 1];
        top--;
        return result;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return data[top - 1];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}

