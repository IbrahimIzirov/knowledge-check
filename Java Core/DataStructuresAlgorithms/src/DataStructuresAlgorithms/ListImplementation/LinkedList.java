package DataStructuresAlgorithms.ListImplementation;

import DataStructuresAlgorithms.ListImplementation.contracts.Linked;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements Linked<T> {

    private Node head;
    private Node tail;
    private int size = 0;

    public LinkedList() {
        head = new Node(null);
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(this::addLast);
    }

    @Override
    public void addFirst(T value) {

        //Създаваме нов Node със стойността, която се подава.
        //Ако списъкът е празен, първоначално и head и tail сочат към стойнността, която се подава.
        //Ако списъка не е празен, задаваме стойността на node.next да сочи към елемента който се намира в началото.
        //В другия случай, обнови стойността на head към новата стойност, и увеличи размера на списъка.

        var node = new Node(value);

        if (head == null || size == 0) {
            head = node;
            tail = node;
        } else {
            head.prev = node;
            node.next = head;
        }

        head = node;
        size++;
    }

    @Override
    public void addLast(T value) {

        //Създаваме нов Node със стойността, която се подава.
        //Ако списъкът е празен, първоначално и head и tail сочат към стойнността, която се подава.
        //Ако списъка не е празен, задаваме стойността на node.next да сочи към елемента който се намира в края.
        //В другия случай, обнови стойността на tail към новата стойност, и увеличи размера на списъка.

        var node = new Node(value);

        if (head == null || size == 0) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
        }

        tail = node;
        size++;
    }

    @Override
    public void add(int index, T value) {

        if (index < 0 || index > size()) {
            throw new NoSuchElementException();
        }

        Node currentNode = head;
        int indexCounter = 0;
        int targetIndex = index - 1;

        Node newNode = new Node(value);

        if (currentNode != null) {
            while (indexCounter < targetIndex && currentNode.next != null) {
                indexCounter++;
                currentNode = currentNode.next;
            }

            if (indexCounter == targetIndex) {
                newNode.next = currentNode.next;
                currentNode.next = newNode;

            } else if (index == 0) {
                newNode.next = head;
                head = newNode;
            }

        } else if (index == 0) {
            head = newNode;
        }
        size++;
    }

    @Override
    public T getFirst() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        return head.value;
    }

    @Override
    public T getLast() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        return tail.value;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size()) {
            throw new NoSuchElementException();
        }

        Node temp = head;

        for (int i = 0; i < size(); i++) {

            if (i == index) {
                return temp.value;
            }

            temp = temp.next;
        }
        return null;
    }

    @Override
    public int indexOf(T value) {
        // Първоначално индекса е с стойност 0
        // завъртаме един while цикъл докато head е различно от null
        // Ако стойността на head е равна с стойността която му подаваме да върне индекса.
        // Ако ли пък не е равна да даде следващата стойност на head и да увеличи индекса с 1.
        // Ако се подаде невалиден индекс да върне -1.

        int index = 0;
        Node current = head;

        while (current != null) {
            if (current.value == value) {
                return index;
            }
            index++;
            current = current.next;
        }
        return -1;
    }

    @Override
    public T removeFirst() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        Node temp = head;
        if (head != null) {
            head = head.next;
        } else {
            throw new NoSuchElementException();
        }
        size--;

        return temp.value;
    }

    @Override
    public T removeLast() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        if (size() != 1) {
            T temp = tail.value;
            tail = tail.prev;
            tail.next = null;
            size--;

            return temp;
        }
        size--;
        return tail.value;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            Node current = head;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                if (hasNext()) {
                    T data = current.value;
                    current = current.next;
                    return data;
                }
                return null;
            }
        };
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private class Node {
        Node prev;
        T value;
        Node next;

        public Node(T value) {
            this.value = value;
        }
    }
}
