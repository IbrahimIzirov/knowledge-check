package DataStructuresAlgorithms.ListImplementation;

import DataStructuresAlgorithms.ListImplementation.contracts.Queue;

import java.util.NoSuchElementException;

public class LinkedQueue<E> implements Queue<E> {
    private Node<E> head, tail;
    private int size;

    public LinkedQueue() {
        size = 0;
    }

    @Override
    public void enqueue(E element) {
        var temp = new Node<E>(element);
        if (isEmpty()) {
            head = temp;
        } else {
            temp.setNext(tail);
            tail = temp;
        }
        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        E element = head.data;
        head = head.next;
        size--;

        if (isEmpty()) {
            tail = null;
        }

        return element;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return head.data;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private class Node<E> {
        public E prev;
        public E data;
        public Node<E> next;

        public Node(E data) {
            this.data = data;
        }

        public E getPrev() {
            return prev;
        }

        public void setPrev(E prev) {
            this.prev = prev;
        }

        public E getData() {
            return data;
        }

        public void setData(E data) {
            this.data = data;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }
}

