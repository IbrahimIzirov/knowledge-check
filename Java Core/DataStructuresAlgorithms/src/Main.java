import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String input = "aabbbccc";

        System.out.println(returnMostlyVisitedSymbol(input));

    }

    public static int returnCharacterCount(String s, char c) {
        int counter = 0;
        for (char symbol : s.toCharArray()) {
            if (symbol == c) {
                counter++;
            }
        }
        return counter;
    }

    public static String returnMostlyVisitedSymbol(String s) {
        int max = Integer.MIN_VALUE;
        int count = 1;
        StringBuilder builder = new StringBuilder();

        Map<Character, Integer> map = new HashMap<>();
        for (char symbol : s.toCharArray()) {
            if (map.containsKey(symbol)) {
                map.put(symbol, ++count);
            } else {
                map.put(symbol, map.getOrDefault(symbol, 1));
                count = 1;
            }
        }
    }

    public static int returnBiggestNumber(int[] array) {
        int output = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            int currentNumber = array[i];
            if (currentNumber > output) {
                output = currentNumber;
            }
        }
        return output;
    }
}