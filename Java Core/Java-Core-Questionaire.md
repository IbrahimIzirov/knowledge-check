# Java Technical Questionnaire

These are the theoretical questions that will most likely be asked during the project defenses and during the technical
part of your job interviews. The idea of these questions is for them to act like a learning guide and provide the
student an accurate way of measuring their understanding of the key concepts behind each course and module.

## **Java Language**

- JAVA - SOURCE CODE - COMPILER - BYTE CODE - MACHINE CODE - CPU - OUTPUT;
- JAVA _ SOURCE CODE - INTERPRETER - OUTPUT

---

### Questions that you must be able to answer in detail

**What is Java?**

Пишете веднъж, стартирайте навсякъде (WORA) или понякога
Пишете веднъж, стартирайте навсякъде (WORE), беше лозунг от 1995 г. [1], създаден
от Sun Microsystems за илюстриране на предимствата на платформата на езика Java . [2] [3] В идеалния
случай това означаваше, че Java програма може да бъде разработена на всяко устройство, компилирана в стандартен
байт код и да се очаква да работи на всяко устройство, оборудвано с Java виртуална машина (JVM). Инсталирането на
JVM или Java интерпретатор на чипове, устройства или софтуерни пакети се превърна в стандартна индустриална практика.

Програмист може да разработи код на компютър и да очаква той да работи на мобилни телефони с активирана Java ,
както и на рутери и мейнфреймове, оборудвани с Java, без никакви настройки. Това имаше за цел да спести на
разработчиците на софтуер усилията да напишат различна версия на своя софтуер за всяка платформа или операционна
система, на която възнамеряват да разполагат.

Език от високо ниво.

Обектно ориентиран програмен език.

---

**What is JDK, JRE and JVM?**

1) `JDK` позволява направата на приложения,JRE само тяхното използване
2) `JRE` Използва се за рънване на програми на всяко устройство или операционна система и да мениджира или оптимизира
   програмната памет.
3) `JVM` Виртуалната машина Java е абстрактен (виртуален) компютър,
   дефиниран от спецификация. Използваният алгоритъм за събиране на боклука и
   всяка вътрешна оптимизация на инструкциите за виртуална машина на Java
   (превеждането им в машинен код) не са посочени. Основната причина за този пропуск е да не
   се ограничават ненужно изпълнителите. Всяко приложение на Java може да се изпълнява
   само в рамките на конкретна реализация на абстрактната спецификация на Java виртуалната
   машина.

---

**Why is `import` in Java?**

Добавя други класове , пакети или интерфейси в даден клас.

---

**What is Generic type in Java? Can you give an example?**

Е тип който приема обект и връща обект независимо от типът му.

The most commonly used type parameter names are:

- `E` - Element (used extensively by the Java Collections Framework),
- `K` - Key
- `N` - Number
- `T` - Type
- `V` - Value
- `S,U,V` etc. - 2nd, 3rd, 4th types

---

**Is data passed by *Reference* or by *Value* in Java?**

- By value

Ако подадем примитивна променлива в метод и въпре има промени с нея , извън метода няма да се отразят защото е
примитивна и съдържа стойността в себе си.
Ако подадем Референтна ,тя предава адреса и всяка промяна ще бъде отразена.

---

**What is the difference between `==` and `equals()`?**

'==' сравнява адреса , equals стойността.

---

**What is the difference between `static` and `final` variables in Java?**

static променлива може да се използва от класа и да бъде променяна , а final променливата
може да се използва от класа но не може да бъдя променена.

---

**Describe the meaning of the `final` keyword when applied to a *class*, *method*, *field* or a *local variable*.**

final класът не може да бъдя наследяван, метода не може да бъдя override-нат но може да бъдя overloaded ,
field i variable не могат да бъдат променяни.

---

**What are the differences between `local`, `static` and `instance` variables?**

local променливата се използва само в рамките на даден метод , конструктор и тн.
static е променлива инстанцирана в класа извън който и да е метод( само едно копие ).
instance променливата се запазва като копие в всеки обект.

---

**What is the difference between primitive and reference type?**

Примитивните не могат да бъдат null, референтните могат.
Примитивните съдържат стоиността в себе си а референтните на даден адрес.

---

**What is autoboxing and unboxing?**

Autoboxing is the automatic conversion that the
Java compiler makes between the primitive types and
their corresponding object wrapper classes. For example, converting
an int to an Integer, a double to a Double,
and so on. If the conversion goes the other way, this is called unboxing.

---

**What is the difference between `String`, `StringBuilder` and `StringBuffer` in Java?**

1) String is immutable.
2) StringBuilder is mutable(override) and hence changes its value.
3) StringBuffer е се ползва предимно когато искаме да принтираме нещо.

---

**What is Java String Pool?**

Специално място в `heap` паметта, където се съхраняват уникални стойности на обекти от тип `String` - стрингови
литерали.
Когато създадем нов литерал чрез директно присвояване, например `String str = "hello";`, Java проверява в String Pool-а
дали
вече съществува идентичен низ. Ако съществува, се използва съществуващата стойност, вместо да се създава нов обект.
При създаване на низове с `new String()`, се създава нов обект, който не използва String Pool-а.

---

**How many ways are there to create a `String` Object in Java?**

- String str = new String(“this is string created by new operator”);
- String one = new String("new");
- String str = “This is string literal”;
- String one = "new";

---

**Is `String` a *Primitive* or a *Derived* Type?**

String is not a primitive type, it is a derived type.

---

**What are the benefits of strings Being *Immutable*?**

Ето защо е полезно низовете (стринговете) в Java да са непроменими:

1) `Безопасност при достъп`: Имутируемите низове се предпазват от нежелани промени, което ги прави лесни за използване в
   различни класове и методи без риск от инцидентно променяне.
2) `Безопасност при многонишкови операции`: Един и същи низ може да се използва едновременно от няколко нишки без риск
   от промяна.
3) `По-малко памет`: Java запазва само уникални стойности в т.нар. String Pool, което намалява използването на памет.
4) `Бързина`: Може да се кешират стойности като хеш кодове, което ускорява операции с много повтарящи се стрингове (като
   ключове в HashMap).

---

**What does immutable mean? How can you design it?**

Неизменим обект е обект, чието вътрешно състояние остава постоянно, след като е изцяло създадено .
Това означава, че след като обектът е присвоен на променлива, не можем нито да актуализираме препратката,
нито да мутираме вътрешното състояние по никакъв начин.

```
public final class Person {
    private final String name;
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
```

Във този пример, `Person` is immutable, защото името и годините са създадени един път през конструктора,
няма сетъри, които да добавят тези стойности и не може да бъде променен след създаването си.

---

**When comparing strings, what’s the difference between `str1 == str2` and `str1.еquals(str2)`?**

Ако е за литерали няма разлика , ако е за обекти == сравнява адреса , а equals стойността.

---

**How can we convert `String` to `Integer` and `Integer` to `String` in Java?**

1) Integer.parseInt(str);
2) String.valueOf(num);

---

**What is `String.format()` used for?**

The java string format() method returns the formatted
string by given locale, format and arguments.

---

**How can we convert a String to a Character array?**

char[] charArray = str.toCharArray();

```
Character[] charArray = str.chars()
                           .mapToObj(c -> (char) c)
                           .toArray(Character[]::new);
```

---

**What is try-with–resources block in Java?**

Изявлението try -with-resources е оператор try, който декларира един или повече ресурси.
Ресурсът е обект, който трябва да бъде затворен, след като програмата приключи с него. Операторът try -with-resources
гарантира, че всеки ресурс е затворен в края на изявлението.

---

**How does Java allocate stack and heap memory?**

Главната разлика между `Stack memory` и `Heap Memory` е, че Stack паметта се използва за
запазването на реда по който са извикани методите и локалните променливи, докато
Heap паметта пази стойността на обектите и ги използва за динамично разпределяне на памет.

1) Stack memory е физическо пространство (в RAM), което се отделя за всяка нишка по време на изпълнение.
   Управлението на паметта в стека следва принципа LIFO (Last-In-First-Out), защото е глобално достъпна.
   В Stack memory се съхраняват variables, references to objects, and partial results, които се изчистват при завършване
   на даден метод. При недостиг на място се хвърля `java.lang.StackOverFlowError`.

2) Създава се при стартирането на JVM и се използва, докато приложението работи. В heap паметта се съхраняват обекти и
   JRE класове. Когато създаваме обект, той заема място в heap паметта, а препратката към него се съхранява в стека.
   Паметта в heap няма подредба като тази в стека и се управлява динамично. Java използва `Garbage collector` за
   автоматично управление на паметта, като премахва неупотребяваните обекти. Ако паметта се запълни, се хвърля грешка
   `java.lang.OutOfMemoryError`.

![Stack and Heap memory](input-images/stack-heap-memory.png)

---

**What is bytecode in Java?**

Bytecode в Java е причината Java да не е независима от платформата, веднага щом се компилира Java програма, се
генерира байт код. За да бъдем по-точни, байт кодът на Java е машинният код под формата на .class файл.

- SOURCE CODE - COMPILER - BYTE CODE - JVM - MACHINE CODE

---

**What is method signature in Java?**

Името на метода + неговите параметри.

Техният тип и подредба на параметрите , return type and exceptions не са част от сигнатурата.

---

**What is return type of methods in Java?**

Във Java, return type на метода показва каква стойност ще бъде върната след като приключи неговото изпълнение.

Ето няколко типа които могат да бъдат върнати:

- `Primitive types`: примерно int, double, boolean, и т.н.
- `Object types`: инстанции на класове, като String, List, или всеки персонализиран обект.
- `Void`: Ако метода не връща никаква стойност, има return type void.
- `Generic types`: За методи които могат да връщат различни типове обекти, като List<T>.

---

**What are varargs?**

Varargs (или променлив брой аргументи) позволява на метод да приема произволен брой аргументи от един и същи тип.
Тази функционалност се обозначава със синтаксиса `...` след типа на параметъра

```
public void printNumbers(int... numbers) {
    for (int number : numbers) {
        System.out.println(number);
    }
}
```

---

**What is lambda expression? Give some examples of lambda expressions.**

- За да създадем ламбда израз, ние посочваме входни параметри (ако има такива)
  от лявата страна на ламбда оператора -> и поставяме израза или блока от
  изрази от дясната страна на ламбда оператора. Например ламбда изразът
  (x, y) -> x + y указва, че ламбда изразът взема два аргумента x и y и връща сумата от тях.

Пример за филтриране на списък:

```
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
names.stream().filter(name -> name.startsWith("A")).forEach(System.out::println);
```

---

**What is a functional interface?**

Functional interface в Java е интерфейс, който съдържа точно един абстрактен метод. Те са основата на функционалното
програмиране в Java и се използват за представяне на анонимни функции с lambda изрази. Всеки функционален интерфейс може
да бъде имплементиран с lambda израз, а Java предоставя и вградени функционални интерфейси като `Runnable`, `Callable`,
`Comparator`, `Function`, `Consumer` и `Supplier`.

```
@FunctionalInterface
public interface RetryableConsumer<T> {

    default void printMethod(T message) {
        hanldePayload(message);
        System.out.println("Default method message: {}", message);
    }
    
    void hanldePayload(T payload);
}

RetryableConsumer<Invoice> createInvoice(final InvoiceService service) {
    return invoice -> service.create(invoice);
```

Резултатът би бил изпринтен обектът Invoice в края на метода.

---

**When to use comparable vs comparator vs comparison with lambdas in Java?**

`Comparable` трябва да се използва, когато сравнявате екземпляри от същия клас.

`Comparator` може да се използва за сравняване на екземпляри от различни класове.

С `lambdas` можете да създавате компаратори на място, което повишава четимостта за еднократни критерии за сортиране.

```
// Използване на Comparator с lambdas.
people.sort((p1, p2) -> p1.getAge() - p2.getAge());
```

---

**What is default method its use?**

A default method is a method defined in an interface that has a default implementation. Default methods were introduced
in Java 8 to allow interfaces to be extended without breaking existing implementations.

With default methods, you can provide a default implementation for a method in an interface, which means that classes
that implement the interface are not required to provide their own implementation. If a class does not provide its own
implementation for a default method, it will use the default implementation defined in the interface. Default methods
are useful for extending existing interfaces without breaking existing implementations. They can also be used to provide
a common implementation for a method that is applicable to all classes that implement the interface.

```
interface MyInterface {
    // A default method with a default implementation
    default void defaultMethod(String name) {
    abstractMethod(name)
        System.out.println("This is the name of second method: {}", name);
    }

    // Abstract method (must be implemented by classes that use this interface)
    void abstractMethod(String name);
}
```

---

**What are we using lambdas and method reference for?**

Lambdas изразите позволяват да се предадат функции (или блокове код) на методи, което подобрява четимостта и краткостта
на кода. Методните референции предлагат удобен синтаксис за позоваване на съществуващи методи, вместо да ги реализирате
отново. Те често се използват при операции като сортиране, филтриране и обработка на колекции.

---

**What is the difference between checked and unchecked exceptions?**

Unchecked are runtime exception, and checked are compile time.

Checked exceptions: Наследяват от `Exception`. Те са свързани с условия, които могат да бъдат
предвидени и изискват обработка, например `IOException`.

Unchecked exception: Наследяват от `RuntimeException`. Те обикновено представляват логически грешки, които не могат да
бъдат предвидени, например `NullPointerException`.

---

**What are multiple catch blocks in Java?**

Блокове за изключения подредени в ред от наи-специфичен към най-общ.

```
try {
    // Код, който може да генерира изключение
} catch (IOException | SQLException e) {
    // Обработка на изключенията
}
```

---

**Is `finally` block always executed?**

ДА.

---

**How is the compiler working?**

Както вече споменахме, процесът на компилация преобразува изходния код на високо ниво в машинен код на ниско ниво,
който може да бъде изпълнен от целевата машина. Освен това съществена роля на компилаторите е да информират
разработчика за извършени грешки, особено свързани със синтаксиса.

Процесът на компилация се състои от няколко фази:

- Лексикален анализ
- Анализ на синтаксиса
- Семантичен анализ
- Генериране на междинен код
- Оптимизация
- Генериране на код

---

**If you have class with two methods that have the same name, but different signatures, will the class compile?**

ДА.

---

**What is the difference between Error & Exception? Why is it preferable not to catch and work with Errors?**

**Checked Exceptions**

- `IOException` – This exception is typically a way to say that something on the network, filesystem, or database
  failed.

**Unchecked Exceptions / RuntimeExceptions**

- `ArrayIndexOutOfBoundsException` – this exception means that we tried to access a non-existent array index, like
  when trying to get index 5 from an array of length 3.
- `ClassCastException` – this exception means that we tried to perform an illegal cast, like trying to convert a
  String into a List. We can usually avoid it by performing defensive instanceof checks before casting.
- `IllegalArgumentException` – this exception is a generic way for us to say that one of the provided method or
  constructor parameters is invalid.
- `IllegalStateException` – This exception is a generic way for us to say that our internal state, like the state of
  our object, is invalid.
- `NullPointerException` – This exception means we tried to reference a null object. We can usually avoid it by
  either performing defensive null checks or by using Optional.
- `NumberFormatException` – This exception means that we tried to convert a String into a number, but the string
  contained illegal characters, like trying to convert “5f3” into a number.

**Errors**

- `StackOverflowError` – this exception means that the stack trace is too big. This can sometimes happen in massive
  applications; however, it usually means that we have some infinite recursion happening in our code.
- `NoClassDefFoundError` – this exception means that a class failed to load either due to not being on the classpath
  or due to failure in static initialization.
- `OutOfMemoryError` – this exception means that the JVM doesn't have any more memory available to allocate for more
  objects. Sometimes, this is due to a memory leak.

---

**What is File? How can you read a file?**

`File` в Java е абстракция, която представлява пътя към файл в системата и предоставя методи за работа с файлове, като
четене и запис.

За да прочетете файл, можете да използвате класовете `FileReader` и `BufferedReader`.

Пример:

```
BufferedReader br = new BufferedReader(new FileReader("file.txt"));

final File file = new ClassPathResource(path).getFile();
```

---

**What is Stream API in Java?**

Една от основните нови функции в Java 8 е въвеждането на функционалността на потока

`java.util.stream` - която съдържа класове за обработка на последователности от елементи.
Централният клас на API е Stream <T> .

Stream API поддържа и паралелна обработка на данни, което подобрява производителността при работа с големи обеми от
информация. Например, можете лесно да извлечете, трансформирате и сортирате данни с минимален код.

---

**Explain stream operations with an example?**

````
public class StreamExample {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        // Example 1: Filter
        List<Integer> evenNumbers = numbers.stream()
                .filter(n -> n % 2 == 0)
                .collect(Collectors.toList());
        System.out.println("Even numbers: " + evenNumbers);

        // Example 2: Map
        List<Integer> squares = numbers.stream()
                .map(n -> n * n)
                .collect(Collectors.toList());
        System.out.println("Squares: " + squares);

        // Example 3: Reduce
        int sum = numbers.stream()
                .reduce(0, Integer::sum);
        System.out.println("Sum: " + sum);
    }
}
````

**Filter**: Операцията за филтриране ви позволява да избирате елементи от Stream потока въз основа на дадено условие.
В този случай филтрираме четните числа от първоначалния списък.

**Map**: Операцията "map" преобразува всеки елемент от Stream потока в друга стойност.
В този случай вдигаме на квадрат всяко число от потока, за да получим нов списък от квадратни числа.

**Reduce**: Операцията за редуциране обединява елементите на Stream потока в един резултат.
В този случай изчисляваме сумата на всички числа в потока, като използваме референцията
на метода Integer::sum като операция за редуциране.

---

**How many types of Stream operations are there?**

1) **Intermediate Operations**: Тези операции преобразуват един поток в друг. Примери са `filter`, `map` и `sorted`. Те
   са
   лениви, т.е. не се изпълняват, докато не се извика терминална операция.

2) **Terminal Operations**: Тези операции произвеждат резултат или страничен ефект и завършват обработката на потока.
   Примери са `collect`, `forEach` и `reduce`.

3) **Short-circuiting Operations**: Тези операции могат да спрат обработката, преди да обработят целия поток, като
   `findFirst` и `anyMatch`.

#### Questions you should have general knowledge of and should answer in a few words

- Is Java code managed or unmanaged code?
- Java is a managed language

---

- What is the role of the JVM?
- JVM има две основни функции: да позволи на Java програми да се изпълняват на
  всяко устройство или операционна система (известна като принцип WORA - Пишете веднъж, стартирайте навсякъде )
  и да управлява и оптимизира програмната памет

---

- What is Ahead-of-time (AOT) compilation and bytecode in Java?
- AOT компилацията е един от начините за подобряване на производителността
  на Java програми и по-специално времето за стартиране на JVM . JVM изпълнява Java
  байт код и компилира често изпълнявания код в роден код. Това се нарича Just-in-Time (JIT) Compilation.
  JVM решава кой код да компилира въз основа на информация за профилиране, събрана по време на изпълнение.

Докато тази техника позволява на JVM да произвежда силно оптимизиран код
и подобрява пикова производителност, времето за стартиране вероятно не е оптимално,
тъй като изпълненият код все още не е компилиран JIT. AOT има за цел да подобри този така наречен период на загряване .
Компилаторът, използван за AOT, е Graal.

---

- What are functional interfaces in Java?
- Всеки интерфейс със SAM (Single Abstract Method) е функционален интерфейс и
  изпълнението му може да се третира като ламбда изрази.

- How can we create our custom pool of objects, like the String Pool?
-

- How does garbage collector work?


- What do you know about threads?


- Can you use streams with primitives?
- За щастие, за да се осигури начин за работа с трите най-използвани примитивни типа - int, long и double - стандартната
  библиотека включва три примитивни специализирани реализации: IntStream , LongStream и DoubleStream .

****

## **Java Object Orientated Programming**

**What are 4 OOP Principles?**

Encapsulation , Inheritance , Abstraction and Polymorphism.

1. `Encapsulation`: Капсулирането е практика на скриване на вътрешните детайли на изпълнението на даден обект и
   предоставяне на добре дефиниран
   интерфейс за взаимодействие с обекта. Тя включва групиране на свързани данни (атрибути) и поведения (методи)
   заедно в рамките на класа и контролиране на достъпа до тях с помощта на модификатори за достъп като
   private, public и protected. Капсулирането насърчава модулността на кода, сигурността на данните и абстракцията,
   като улеснява управлението и поддръжката на сложни системи.

2. `Inheritance`: Наследяването е механизъм, който позволява на един клас (подклас) да наследява свойства и поведение от
   друг клас (суперклас).
   Подкласът може да разшири суперкласа, като добави нови характеристики или пренареди съществуващите.
   Той дава възможност за повторно използване на кода, насърчава организацията на кода и установява връзка "е-а" между
   класовете.
   В Java за реализиране на наследяването се използва ключовата дума **extends**.

3. `Polymorphism`: Полиморфизмът е способността на даден обект да приема различни форми или поведения в зависимост от
   контекста.
   Тя позволява обекти от различни класове да бъдат третирани като обекти от общ суперклас, което осигурява гъвкавост и
   разширяемост на кода.
   Полиморфизмът се постига чрез надграждане на методи и претоварване на методи. Презаписването на методи се случва,
   когато подкласът предоставя своя реализация за наследен метод, докато претоварването на методи включва дефиниране на
   множество
   методи с едно и също име, но с различни списъци с параметри. Полиморфизмът дава възможност за по-гъвкав и модулен
   код.

4. `Abstraction`: Абстракцията включва създаване на опростени представяния на сложни системи, като се фокусира върху
   основните характеристики
   и се скриват ненужните детайли. Тя позволява на разработчиците да дефинират абстрактни класове и интерфейси, които
   определят набор от методи,
   без да предоставят имплементация. Абстрактните класове предоставят частична реализация, докато интерфейсите определят
   договор,
   към който имплементиращите класове трябва да се придържат. Абстракцията помага за управление на сложността,
   насърчаване на повторното използване на кода и създаване на модулни проекти.

---

**What is a class?**

````
public class Car {
    // Attributes
    private String make;
    private String model;
    private int year;

    // Constructor
    public Car(String make, String model, int year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }

    // Methods
    public void start() {
        System.out.println("The car is starting.");
    }

    public void accelerate() {
        System.out.println("The car is accelerating.");
    }

    public void stop() {
        System.out.println("The car is stopping.");
    }

    // Getters and Setters
    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
````

Класовете служат като градивни елементи на програмите в Java, като позволяват на разработчиците да дефинират свои
собствени типове
и да организират свързания код в единици за многократна употреба. Те играят централна роля при създаването на обекти
и прилагането на обектно-ориентирани концепции като капсулиране, наследяване, полиморфизъм и абстракция.

---

**What is a record in Java 17?**

В `Java 17` беше въведена нова функция, наречена `record`. Записът е компактен и кратък начин за деклариране на клас,
който се използва предимно за съхраняване на неизменни данни.

````
public record Person(String name, int age) {
    public void greet() {
        System.out.println("Hello, I'm " + name);
    }
}
````

В този пример класът Person е деклариран като запис. Той има две полета: `name` от тип String и `age` от тип int.
При тази декларация на запис компилаторът на Java автоматично генерира следното:

1. Конструктор, който приема name и age като параметри и инициализира полетата.
2. Методи за достъп `getters` за полетата name и age.
3. Методи `equals()`, `hashCode()` и `toString()`, базирани на полетата.

Records могат да се използват по подобен начин на обикновените класове:

````
Person person = new Person("John Doe", 30);
System.out.println(person.name()); // Accessing field using getter
System.out.println(person.age());  // Accessing field using getter
person.greet(); // Invoking additional method
````

---

**What is difference between class and record in java 17?**

1. `Immutability`: По подразбиране **records** в Java са неизменни, което означава, че състоянието им не може да се
   променя,
   след като са инстанцирани. Всички полета в един запис са имплицитно окончателни и генерираният конструктор
   инициализира тези полета.
   За разлика от тях, **classes** могат да бъдат променливи или непроменливи, в зависимост от начина, по който са
   дефинирани.

2. `Inheritance`: **records** не могат да бъдат директно унаследявани или **extend** от други класове.
   Те имплицитно разширяват класа java.lang.Record. От друга страна, **classes** могат да бъдат унаследявани или *
   *extend** от други класове.

3. `In Addition`: В обобщение, **classes** са по-универсални и осигуряват по-голяма гъвкавост по отношение на
   дефинирането на поведението, наследяването и променливостта.
   **records**, от друга страна, са специализирани конструкции, оптимизирани за съхранение на неизменни данни и
   автоматично генерират общи методи,
   намалявайки шаблонния код. Те са особено полезни при работа с класове, ориентирани към данни,
   или при създаване на прости и кратки представяния на неизменни данни.

---

**What is an object?**

Обектите имат състояния(state) и поведения(behavior).

Обектите са градивните елементи на програмите на Java и се използват за моделиране и взаимодействие с обекти в
проблемната област.
Всеки обект е самостоятелна единица, която капсулира данни (**атрибути или полета**) и поведения (**методи**), свързани
с тези данни.
Обектите могат да комуникират помежду си чрез извикване на методи и обмен на данни.

Обектите са от съществено значение в програмирането на Java, тъй като позволяват прилагането на капсулиране,
наследяване, полиморфизъм и абстракция - основните принципи на обектно-ориентираното програмиране.
Те помагат за организирането и управлението на сложни системи, като ги разбиват на по-малки, многократно използваеми и
модулни единици.

---

**What are the access modifiers in Java?**

Public,Private,Protected and Default = without modifier means `package-private`

1. `public`: Модификаторът за публичен достъп позволява неограничен достъп до даден клас, метод, променлива или
   конструктор.
   Той осигурява най-високото ниво на достъпност.

2. `protected`: Модификаторът за защитен достъп позволява достъп в рамките на един и същ пакет, както и от подкласове (
   дори ако те са в друг пакет).
   Обикновено се използва при прилагане на наследяване и осигурява ограничено ниво на достъпност.

3. `default`: Ако не е посочен модификатор за достъп, членът има достъп по подразбиране. Това означава, че той е
   достъпен в рамките на същия пакет, но не и извън него.
   Той осигурява достъпност на ниво пакет.

4. `private`: Модификаторът за достъп private (частен) ограничава достъпа само в рамките на същия клас.
   Той предлага най-рестриктивното ниво на достъпност и обикновено се използва за капсулиране и скриване на детайли от
   изпълнението.

 Access Modifier | Class | Package | Subclass | Any Class 
-----------------|-------|---------|----------|-----------
 public          | Yes   | Yes     | Yes      | Yes       
 protected       | Yes   | Yes     | Yes      | No        
 default         | Yes   | Yes     | No       | No        
 private         | Yes   | No      | No       | No        

---

**Explain *static initializers* in Java?**

Статичният инициализатор е кодов блок с име `static`.

````
static {
    // Static initialization code
}
````

1. `Execution Timing`: Статичният инициализатор се изпълнява, когато класът се зарежда от Java Virtual Machine (JVM).
   Изпълнява се само веднъж, независимо от това колко обекта от класа са създадени. Изпълнява се, преди класът да бъде
   използван
   или да бъде осъществен достъп до статични членове.

2. `Order of Execution`: Ако в един клас има няколко статични инициализатора, те се изпълняват последователно отгоре
   надолу.

3. `Usage Scenarios:`: Статичните инициализатори обикновено се използват за инициализиране на статични променливи
   или за изпълнение на всякакви еднократни задачи за настройка, които трябва да се извършат преди използването на
   класа.
   Например инициализиране на константи, зареждане на конфигурационни файлове, установяване на връзки с бази данни или
   инициализиране на статични структури от данни.

---

**Explain *overloading* in Java?**

Методи с еднакви имена и различни параметри(параметрите също могат да бъдат еднакви но на различни позиций).

---

**Explain *overriding* in Java?**

Ако клас наследява метод от своя суперклас, тогава има шанс да замени метода, при условие че той не е маркиран като
окончателен.

Предимството на заместването е: способността да се дефинира поведение, което е специфично за типа подклас, което
означава, че подкласът може да реализира метод на родителски клас въз основа на неговите изисквания.

В обектно-ориентирани термини, заместването означава да се замени функционалността на съществуващ метод.

---

**What is `abstract class` and `interface`? What is the difference between them? What do they have in common?**

Abstract Class A class which contains the abstract keyword in its declaration is known as abstract class.
Abstract classes may or may not contain abstract methods, i.e., methods without body ( public void get(); )
But, if a class has at least one abstract method, then the class must be declared abstract.
If a class is declared abstract, it cannot be instantiated.
To use an abstract class, you have to inherit it from another class, provide implementations to the abstract methods
in it.
If you inherit an abstract class, you have to provide implementations to all the abstract methods in it.

---

**How do we achieve strong cohesion and loose coupling?**

Всеки клас да изпълнява специфично една единствена фунция.

---

**What is *composition*?**

Композицията е дизайнерска техника в Java, за да се реализира взаимоотношение
"има връзка". Наследяването на Java се използва за целите на повторното използване на кода и
същото можем да направим, като използваме наследяване. Съставът се постига чрез използване на
променлива на екземпляр, която се отнася до други обекти. Ако обектът съдържа другия обект и съдържащият се
обект не може да съществува без съществуването на този обект, тогава той се нарича композиция. В по-конкретни думи
съставът е начин за описване на препратка между два или повече класа, като се използва променлива на инстанцията и
екземпляр трябва да бъде създаден преди да се използва.

---

**Explain Multilevel inheritance?**

Multilevel inheritance is a type of inheritance in object-oriented programming (OOP) where a derived class (subclass) is
created from another derived class, which itself was derived from a base class (superclass). In multilevel inheritance,
each derived class inherits the characteristics of the class above it in the hierarchy. This means that a subclass not
only has all the features of its immediate superclass, but also those of all its ancestors up the hierarchy chain.
Here's an example to illustrate multilevel inheritance:

```
// Base class (superclass)
class Animal {
    void eat() {
        System.out.println("Eating...");
    }
}

// Derived class 1
class Dog extends Animal {
    void bark() {
        System.out.println("Barking...");
    }
}

// Derived class 2 (subclass of Dog, and thus also inherits from Animal)
class Bulldog extends Dog {
    void guard() {
        System.out.println("Guarding...");
    }
}

public class Main {
    public static void main(String[] args) {
        // Creating an instance of Bulldog
        Bulldog bulldog = new Bulldog();
        
        // Calling methods from Animal, Dog, and Bulldog
        bulldog.eat();   // Inherited from Animal
        bulldog.bark();  // Inherited from Dog
        bulldog.guard(); // Defined in Bulldog
    }
}
```

Now, an instance of Bulldog can access all the methods of its immediate superclass (Dog), as well as all the methods of
its ancestor superclass (Animal).

---

**What are `final` classes in Java?**

Тяхната цел е да не бъдат наследявани. Aко се опитаме да ги наследим ще ни даде compile time еrror.

---

**Can we have overloaded methods with different return type?**

No, we cannot have overloaded methods with only different return types. Overloaded methods must have the same method
name and parameters, but they can have different return types only if the method parameters are also different. The
reason for this is that the return type alone is not enough information for the compiler to determine which method to
call at compile time. For example, consider the following code:

```
public class Calculator {
    public int add(int x, int y) {
        return x + y;
    }

    public double add(int x, int y) {  // Compilation error: Duplicate method add(int, int) from type Calculator
        return (double) (x + y);
    }
}
```

In this example, we have two `add()` methods with the same parameters, but different return types (int and double). This
will cause a compilation error because the compiler cannot determine which method to call based on the return type
alone.

---

**What do we use `new`, `this` and `super` for?**

`new` за създаване на инстанция на обект

`this`  извиква поле от дадения клас

`super`  извиква метод или поле от бащиния клас

---

**Can we override the static method? Why we can't do that?**

No, it is not possible to override a static method in Java. A static method is associated with the class and not with an
object, and it can be called directly on the class, without the need of creating an instance of the class.

When a subclass defines a static method with the same signature as a static method in the superclass, the subclass
method is said to hide the superclass method. This is known as method hiding. The subclass method is not considered an
override of the superclass method, it is considered a new method and it hides the superclass method, but it doesn't
override it. In summary, since a static method is associated with the class, not an object and it is directly callable
on the class, it is not possible to override a static method in Java, instead, it is hidden by subclass method with the
same signature.

---

**Can we change the access modifier of a method, while overriding it?**

На по високо ниво може.

--- 

**How many constructors one class can have? What are the differences between them?**

65527 , разлките ще са в параметрите и access модифайъра.

---

**When do we want to make a constructor `private`?**

Не можем да създадем обект извън класа.

---

**Can a method, inherited from a interface, have a `private` access modifier?**

Не може.

---

**What are annotations in Java?**

`Java Annotation` е таг, който представлява метаданните, т.е. прикрепен с
клас, интерфейс, методи или полета, за да посочи допълнителна информация,
която може да се използва от Java компилатора и JVM.

Анотациите в Java се използват за предоставяне на допълнителна информация,
така че това е алтернативна опция за XML и Java интерфейси на маркери.
 
---

**What are covariant return types in Java? Why we can use them with overriding?**

За да повторим, ковариантните типове за връщане позволяват на метод от подклас да замени метод от суперклас,
като връща по-специфичен (производен) тип от оригиналния метод. Тази функция позволява по-голяма гъвкавост при
заместването на методи,
като позволява типът на връщане на заместващия метод да бъде подтип (производен клас) на типа на връщане на замествания
метод.

````
class Animal {
    Animal getAnimal() {
        return new Animal();
    }
}

class Dog extends Animal {
    @Override
    Dog getAnimal() { // Covariant return type
        return new Dog();
    }
}
````

Във този случай обекта `Dog` унаследява метода `getAnimal()` от класа `Animal` и връща обект `Dog`,
който е много по-специфичен тип от типа на връщане на унаследения метод.

---

****

## **Data Structures and Algorithms**

### Questions that you must be able to answer in detail

**What are `data structures`?**

В компютърните науки структурата на данните е формат за организация на данни, управление и съхранение,
който позволява ефективен достъп и модификация. По-точно, структурата на данните е съвкупност от стойности на данните,
връзките между тях и функциите или операциите, които могат да бъдат приложени върху самите данните.

---

**What is a `linear data structure`?**

Линейни структори от данни са тези, които се състоят от множество елементи
които са подредени в някаква последователност, тоест елементите имат някакъв ред.
Такива структури са: List, Stack and Queue.

---

**What operations can we perform on data structures?**

- `Insertion` − Добавяне на нов елемент в дата от структурите.
- `Deletion` − Изтриване на вече съществуващ елемент от дата структурите.
- `Traversal` − Достъп до всеки елемент от данни, за да може да бъде обработен.
- `Searching` − Намиране на локацията на даден елемент, ако той съществува.
- `Sorting` − Подреждане на данните в определен ред.

---

**Explain what is a `linked-list`?**

Той също е линейна структура от данни, като представлява последователност от Nodes(нодове),
всеки от тези нодове съдържа някаква данни и връзка за съседните от него нодове.
Има два типа свързани списъци - `Single linked list` и `Double linked list`.
Разликата е в това че при единично свързания списък връзките(нодовете) са само към следващият елемент и референция е
само към първия елемент от списъка,а при двойно свързания списък има връзки както към предходния така и към
следващия елемент от списъка и референцията има към първия и последния елемент от списъка.

---

**Explain what is a `stack`?**

Линейна структура от данни в която елементите се оправляват от принципа LIFO(Last In First Out),
Тоеа много наподобява купчина с чинии, където може да слагаме чиния само най-отгоре и да взимаме само
от най-отгоре. Имплеметира се или посредством масив или посредством свързан списък.
Една от основните употреби на стека е за съхраняване на веригата от извикани методи в JVM,
при възникване на иключение се отпечатва StackTrace на изключението, което показва последователността
от извикани методи в изключението.

![Stack in Java](input-images/working-of-stack-in-java.gif)

---

**- Explain what are `queues`?**

Линейна структура от данни в която елементите се оправляват от принципа FIFO(First In First Out),
Това много наподобява опашка от хора, където опашката има начало и край, като новите елементи се добавят
винаги в края, а пък останалите елементи се вадят от началото. Имплеметира се или посредством масив или посредством
свързан списък.
Операцията за добавяне към края е константна и премахване от началото също е константна, това е така,
защото имаме референции и към началото и към края на опашката.

![Queues in Java](input-images/working-of-java-queue.gif)

---

- **What is the difference between `Array`, `ArrayList` and `LinkedList` in Java?**

LinkedList изисква повече памет, тъй като той пази допълнителна памет за връзките между елементите.
Когато добавяме или премахваме елементи от свързания списък, това не води до пренареждане на останалите
елементи за разлика от Array. Като следствие от това нещо LinkedList заема точно толкова памет, колкото
елементи има в него и може лесно да добавя нова памет в зависимост от това дали расте или намалява.
При Array заемането на нова памет е свързано с елементите които има. Най-големия недосатък на LinkedList
е, че достъпването на елемент по индекс е бавно(С линейна сложност), при Array това е с константна сложност.

---

**`ArrayList` vs `LinkedList` which is better for different operations?**

- Основотно предимство на `ArrayList` е, че достъпът на елемент по индекс е с константна сложност.
- Основното преимущество на `LinkedList` е, че операциите по добавяне и премахване на елементи в началото и края,
  е много бързо.

---

**What is the difference between `HashMap` and `TreeMap`?**

- `HashMap` - имплементиран е посредством хаш таблица и както при HashSet стойностите по
  добавяне, извличане, сравняване и премахване (put/get/contains/remove) са константни.

- `TreeMap` - имплементира се посредством балансирано дърво и ключовете в него са подредни под определен компаратор.
  Сложностите в него са лагоритъм от n (Ologn).

---

**What is the implementation of the `HashMap`? How does it handle collisions?**

Internally, a `HashMap` uses an array of "buckets" (or bins) to store entries. Each entry in the HashMap is a key-value
pair, represented by a Map.Entry object. When a key is inserted, HashMap computes the hash code of the key, which is
used to determine the index (bucket) where the key-value pair should be placed.

Time Complexity:

- `Average Case`: Insertion, deletion, and retrieval operations all have an average time complexity of O(1) because of
  the
  direct indexing enabled by the hash function.
- `Worst Case`: In cases of high collision (e.g., poor hash distribution or many keys hashing to the same index), the
  time
  complexity can degrade to O(n), where `n` is the number of elements in the map, because operations might require
  iterating over a linked list (if chaining is used).

---

**What is `hashing`?**

Hashing - или по друг начин казано "хеширане", представлява процес при който един по сложен тип от данни се превръща
в друг по - прост и унифициран.

Hashing Process:

1) When you call `put(K key, V value)` to insert a key-value pair into a HashMap, the hash code of the key is computed
   using the hashCode() method.
2) The hash code is then transformed (using a method like spread() or hash()), and this transformed hash is mapped to an
   index in the array (i.e., bucketIndex = hash % arraySize).
3) The entry (key-value pair) is inserted into the corresponding bucket (array element) at the computed index.

---

**What is `collisions`?**

Collisins - или по друг начин казано "колизия", представлява процес при който две промениливи имат еднакъв хеш код.
Единия от начините за разрешаване на колизии е навързването или `separate chaining`. При него, когато на един елемент
е отредено място, което вече е заето от друг се образува свързан списък от двата елемента.
Друг подхват е `open addressing`, при него ако на един елемент е отредено място, което вече е заето от друг, то той
заема следващото свободно място в масива.

```
HashMap<String, String> map = new HashMap<>();

map.put("1", "value1");
map.put("1", "value2");
map.put("1", "value3");

final result will be:
{
    "1" => "value3"
}
```

---

**Describe how hash map work. Provide some pseudo code.**

Key Concepts:

1) Hashing: The hash function converts a key into an index (hash code) in an array.
2) Collision Handling: When multiple keys map to the same index, the HashMap uses chaining (linked lists) to store
   multiple key-value pairs in the same bucket.
3) Array-backed: The HashMap uses an array to store buckets of key-value pairs, with each bucket potentially holding
   multiple entries if collisions occur.

---

**What is а `algorithm`?**

Алгоритъм е последователност от инструкции или стъпки, които трябва да бъдат изпълнени, за да решим някакъв проблем.
Често тези алгоритми работят с входни данни, и произвеждат някакъв резултат които са изходните данни от алгоритъма.

---

**What is `algorithm analysis`?**

Анализ на алгоритъма е процеса в който се опитваме да предвидим, какви ресурси са необходими на алгоритъма.
Най-често става дума за това колко време е необходими и каква памет е нужна за алгоритъма.

---

**Why do we need to do `algorithm analysis`?**

Correctness(Коректност) - на практика това означава дали алгоритъма работи правилно

1) Счита се че един алгоритъм работи вярно,ако той връща коректна стойност на всички входни данни.
2) Ако алгоритъма връща неверен резултат, дори и за 1 от стойностите на входните данни, то той не е коректен.
   Complexity(Сложност) - на практика това означава времето и мястото, които са необходими на алгоритъма,
   като функция на размера на входните данни.

 ---

**What are `asymptotic notations`?**

Една от асимтотичние нотации е нотацията Big O. Чрез тази функция се представя сложноста на алгоритъма
с който работим, те съответно могат да бъдат Константни, Логаритмични, Линейни, Линеаритмични, Двоични, Квадратични и
Факториел

![Big O Chart](input-images/big-O-complexity-chart.png)
---

**What is a `recursive function`?**

Един метод вика сам себе, и това нещо продъжава докато, не намери някакъв базов отговор.
Ако не се зададе базовият случай, рекурсията може да продъжли безкрайно, което
от своя страна ще доведе до StackOverFlow.

От гледна точка на програмирането рекурсивната функция може да бъде дефинирана като рутина,
която се извиква директно или индиректно. Директното извикване може да бъде дефинирано когато
един метод извика себе си. Индиректното от своя страна може да бъде осъществено когато два метода се
викат един друг до приключване на някакво условие.
Използвайки рекурсивен алгоритъм, някои проблеми могат да бъдат решени доста лесно.
Едно критично изискване на рекурсивните функции е терминираща точка или базовият случай.
Всяка рекурсивна програма трябва да има базов случай, за да се увери, че функцията ще се прекрати.
Липсващият базов случай води до безкрайно превъртане на поведението на метода.

---

**What is `linear searching`?**

Идеята на линейното търсене е, че претърсваме масива от ляво на дясно, като проверяваме
всеки един елемент дали отговаря на дадено условие. Пример за такова търсене е foreach цикъла.
Сложността на този алгоритъм при най - лошият сценарий е O(n). - когато елемента се намира накрая на масива.
При най - добрият му сценарий сложността е константна защото, елемента който търсим би бил на първа позиция.

---

**What is `binary searching`?**

За този алгоритъм има едно важно условие и то е масива предварително да бъде сортиран.
Идеята на алгоритъма е, че след като масива е сортиран можем да намерим неговия среден елемент.
Намирайки средния му елемент го сравняваме с това което търсим и ако е съвпадение връщаме true.
Ако нямаме съвпадение продължаваме с търсенето или в лявата половина или в дясната половина,
в зависимост от това дали търсеният елемент е по - малък от средния или по - голям.
Тази техника която се прилага се нарича разделяй и владей.

---

**What is a `tree`?**

Дървото е абстрактна структура от данни, която се подчинява на определени правила.
Дървото, доори и да е празното, все още се води за дърво. Node, който е с 0 или повече деца,
също е дърво сам по себе си.

---

**Terminology of a `tree`?**

- `Node, Edge` - Връзките между нодовете се наричат Edge(ребра).
- `Root, Child, Parent` - Корен(root), е елемента който няма родител сам по себе си, или това е първия елемент.
  Дете(child), е всеки един Node, който има родител спрямо него.
  Родител(parent), е всеки Node, който има деца.
- `Siblings` - Nodes, които са деца на един и същи родител.
- `Descendant(наследници)` - Всички Nodes, които могат да бъдат достигнати от определен родител.
- `Ancestor` - Nodes, които са достижими от дадено дете, когато вървим нагоре посока корена.
- `Leaf(листо)` - Node, който няма деца.
- `Node Height` - Най-дългия път надоло, като започнем от корена, с номерация от 1.
- `Node Depth` - Като започнем от връзките между даден Node, и коренът на това дърво.

---

**What is post-order, pre-order, in-order traversal?**

Когато говорим за DFS в конкретиката на бинарно дърво, има три начина да използваме елемента.
Има значение кога точно се случва операцията в случая с принтиране при рекурсивните извиквания.
При едно и също дърво, тези методи ще дадат различен резултат.

- `pre - order` - Ако приложиме този метод, ще се движим от корена към дъното, и от ляво на дясно.

Top -> Bottom

Left -> Right.

Можем да използваме елемента преди извикванията:

1) Първо ще бъде принтирана стойността на елемента
1) второ ще бъде изпълнено рекурсивно извикване на ляво.
2) трето ще бъде изпълнено рекурсивно извикване на дясно.

Пример:
Когато извикаме нашето дърво с корена(root), първо ще принтира елемента който е в корена,
след което ще бъде извикано рекурсивно извикване на ляво, отново ще се принтира стойността
и пак ще се направи извикване на ляво и ще слезне още надоло до ниво в което има само листа.
Когато стигне до такова ниво ще направи извиквания на ляво и на дясно и ще ги принтира в последователност:
корен -> ляво -> дясно. Така ще започне да се връща нагоре докато не изпринтира всички елементи на дървото.

````
preOrder(node):
  print node.value
  preOrder(node.left)
  preOrder(node.right)
````

- `post - order` - Ако приложиме този метод, ще се движим от дъното нагоре, и от ляво на дясно.

Bottom -> Top

Left -> Right.

Можем да използваме елемента след извикванията:

1) първо ще бъде изпълнено рекурсивно извикване на ляво.
2) второ ще бъде изпълнено рекурсивно извикване на дясно.
3) и след това ще бъде принтирана стойността.

Пример:
Когато извикаме нашето дърво с корена(root), първо ще провери всичко на ляво, след което ако такъв елемент,
на него също ще направи извикване на ляво и чак когато стигне до листо, ще започне да принтира.
След това рекурсията ще отстъпе една стъпка назад и ще провери извикване на дясно, ще провери дали елемента е листо
и ако е ще принтира резултата. Съшите стъпки ше се повторят за остатъка от дървото.

````
postOrder(node):
  postOrder(node.left)
  postOrder(node.right)
  print node.value
````

- `in - order` - Ако приложиме този метод, ще се движим от лявата част към корена и после в дясно за всяко под дърво.

Left -> Node -> Right

Можем да използваме елемента между извикванията:

1) Първо ще бъде изпълнено рекурсивно извикване на ляво.
1) Второ ще бъде принтирана стойността на елемента.
2) Трето ще бъде изпълнено рекурсивно извикване на дясно.

Пример:
Подавайки корена на дървото, първо ще изпълним извикванията на ляво т.е. метода за извикване на ляво ще продължи,
докато не стигне до листо и това ще бъде първия принтиран елемент, след което ще отстъпи назад една стъпка, ще принтира
този елемент и ще бъде изпълнено извикване на дясно, и пак ще принтира този елемент.
Това действие ще продължи докато не бъде принтирано изцяло дървото.

````
inOrder(node):
  inOrder(node.left)
  print node.value
  inOrder(node.right)
````

![Traversal](input-images/145_transverse.png)
---

**What is a `binary tree`?**

Бинарното дърво е частен случай на обикновеното дърво, тук има още едно допълнително правило -
децата винаги могат да бъдат 0, 1 или 2, но никога повече. Дефиницията важи за всеки един Node.
Прието е двете деца да се разглеждат като ляво и дясно в света на програмирането.

---

**What is a `binary search` tree?**

Дървото се нарича бинарно дърво за търсене, когато можем да имаме 0, 1 или най-много 2 деца, тук
имаме допълнителното правило стойността на всеки един нод е по - висока от стойнотта на всички нодове, които
стоят от лявата му част, и съответно по ниска от всичко които стоят от дясната му част.
Операциите които можем да извършваме върху едно бинарно дърво за търсене са: Добавяне, премахване, търсене и обхожждане.

---

**Describe how `heaps` work. Write some pseudo code**

Всичките им нива са запълнени с изключение на последното ниво. Всеки един node, е по - добър
от наследниците си в ляво или дясно. Тук сложностите по търсене е линейна, добавянето и премахването
на елемент са с лагоритъм от n - сложност. докато при вземането на елемент сложността е константна.
Това е така защото най-добрият елемент стои винаги отгоре.
В джава Binary Heap е имплементирано посредстовом структурата PriorityQueue<T>.

--- 

**Think about and evaluate complexity of different search and sort algorithms. Explain when using array is better in
some cases.**

На графиката е представена схема с сложностите на всички търсения и сортировки.
Ясно се вижда, че двоичното търсене е с много по - малка сложност и е по - добрият вариант за търсене.
От алгоритмите за сортиране Merge sort и Quick sort са с най-малка сложност и най - бързи съответно за изпълнение.

![Time complexity](input-images/c950295.png)

---

**What is `Bubble Sort`?**

Bubble sort is a sorting algorithm that works by comparing the adjacent elements and swapping them based on ascending or
descending sorting criteria until the array is in the expected order.

![Bubble Sort Animation](input-images/bubble-sort-animation-swtestacademy-bg.gif)

---

**What is `Selection Sort`?**

Selection sort is a sorting algorithm that works by selecting the biggest number in an unsorted array and moving it to
its final location. In this way, we can sort the array in ascending order.

![Selection Sort Animation](input-images/selection-sort-amination.gif)

---

**What is `Insertion Sort`?**

Insertion sort is a sorting algorithm that works by inserting the index to be sorted element to its correct position
based on comparisons with the other elements.

Before starting the sorting, we assume that the index 0 is already sorted and then we have an iteration from index 1 to
n-1 and in each iteration, we have an inner loop from right to left to do comparisons to insert the unsorted element to
the correct position.

![Insertion Sort Animation](input-images/insertion-sort.gif)

---

**What is `Merge Sort`?**

Merge sort is a very good example of Divide and Conquer2 algorithms. We recursively split the list in half until we have
lists with size one. We then merge each half that was split, sorting them in the process.

![Insertion Sort Animation](input-images/merge_sort.gif)

---

**What is `Quick Sort`?**

Quick sort uses the divide-and-conquer approach based on the idea of choosing a pivot element from the array and
partitioning the surrounding array where the Left side of pivot contains elements that are smaller than the pivot and
Right side contains elements greater than the pivot.

The different ways to perform quick sort are:

- The last element is selected as the pivot (implemented below).
- The first element is selected as the pivot.
- The middle element is selected as the pivot.
- A random element is selected as the pivot.

![Insertion Sort Animation](input-images/quick_sort_partition_animation.gif)

---

**What is `Heap Sort`?**

Heap Sort is a comparison-based sorting algorithm that uses a binary heap data structure. It follows the
Divide-and-Conquer paradigm and is an efficient, in-place algorithm with a time complexity of O(n log n) for both
average and worst cases.

1) Building the Heap:

The heap is a complete binary tree, and there are two types of heaps: Max-Heap and Min-Heap. In Max-Heap, the largest
element is at the root, while in Min-Heap, the smallest element is at the root.
For Heap Sort, a Max-Heap is typically built, as it helps in extracting the largest element.

2) Heapifying:

To ensure the heap property (where a parent is larger than its children in a max-heap), heapify is applied. The
heapify function ensures that the subtree rooted at an index satisfies the heap property. If it's violated, the
function swaps the node with the larger child and repeats the process recursively.

3) Extracting Elements:

The root of the heap (the largest element in a Max-Heap) is removed and placed at the end of the array.
After removing the root, the last element in the heap is moved to the root, and the heap property is restored by calling
heapify on the root. This process is repeated for the remaining elements until the heap is empty.

![Insertion Sort Animation](input-images/Heap_sort_example.gif)

---

#### Questions you should have general knowledge of and should answer in a few words

**What is a graph? Provide practical examples of graphs.**

Графът като структура от данни представлява връзките между отделните елементи на дадено множество.
Всеки член на това множество се нарича връх, а връзката между два върха се нарича ребро.
Честата употреба на графите в практиката е довела до задълбочени изследвания в теория на графите,
в която са известни огромен брой задачи за графи и за повечето от тях има и добре известно решение.
Имплементацията на граф може да асоциира към всеки връх дадена стойност,
като буквено-цифрово означение или дадена стойност(константа, капацитет, дължина и т.н.).

---

**How does depth-first traversal work?**

DFS или така нареченото обхождаме по дълбочина. Като обхождането започва от root елемента към листата на дървото,
и след това обхождаме и другите разклонения. Имплементацията на това обхождане най често се прави или посредством
рекурсия или чрез Stack(стек).

---

**Provide a practical example of DFS. Explain it.**

````
    public void dfs(node) {
        for (TreeNode<T> child : children) {
            dfs(child);
        }
    }
````

функция на която в началото придаваме root елемента, след което с един цикъл -
за всяко едно дете на този node извикваме рекурсивно същия метод с съответното дете.

---

**How does breadth-first traversal work?**

BFS или така нареченото обхождане по широчина. Тук минаваме през всички елементи по нива т.е.
първо обхождаме root, след което неговите деца, след това и техните деца и т.н. Обикновенно се имплементира
посредством Queue(опашка).

---

**Provide a practical example of BFS. Explain it.**

fun bfs (node):
queue.enqueue(node)
while queue is not empty:
next = queue.dequeue
use node
for each child of next.children:
queue.enqueue(child)

Имаме една функцията на която подаваме root елемента като node, и след това го добавяме към
опашката. Със един While - цикъл, докато опашката има елементи вътре в нея, първо изваждаме елемента,
използваме го, след което с един for each цикъл, добавяме всички негови деца в тази опашка.
След това се връщаме и продължаваме while цикъла докато абсолютно всичките елементи не бъдат посетени.

---

**What is interpolation search?**

---

**How do you choose between `HashSet` vs. `TreeSet`?**

TreeSet е структура която запазва уникални елементи, като ги запазва подредени. Имплементацията на тази структура в
езикът джава е посредством red/black дървета, като това е вид самобалансиращи се дървета.

---

**Explain divide and conquer algorithms?**

Тя се прилага за разрешаването на някой по сложен проблем, като идеята е да разбирем сложният проблем
на по - малки проблеми, които обаче са от абсолютно същия характер като големия проблем.
Идеята е прилагайки тази техника много пъти, накрая да стигнем до някой прост проблем, който
можем директно да решим. Така описана до момента тази техника много наподобява рекурсивното извикване.
И поради тази причина тази техника се имплементира посредством рекурсия.

---

**What is dynamic programming?**

Динамичното програмиране е едновременно математически метод за оптимизация и метод за компютърно програмиране.
И в двете ситуации дадена сложна задача се опростява чрез разделяне на по-прости подзадачи.
По това динамичното програмиране прилича на известния метод „разделяй и владей“.
Разликата между тях е, че при динамичното програмиране две или повече различни задачи могат да имат обща подзадача,
а при метода „разделяй и владей“ това не е възможно.
Затова последният е обикновено много по-прост от динамичното програмиране.

---

**What is functional programming?**

While Java is traditionally an object-oriented language, it has adopted many features from functional programming,
especially since Java 8. These features allow developers to write more concise, flexible, and declarative code, focusing
on what to do rather than how to do it.

How Java Implements Functional Programming?

1) `Lambda Expressions`:

```
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

// Using lambda to print each name
names.forEach(name -> System.out.println(name));
```

2) `Streams`:

```
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

// Using Stream API to filter even numbers and square them
List<Integer> squaredEvens = numbers.stream()
                                    .filter(n -> n % 2 == 0)
                                    .map(n -> n * n)
                                    .collect(Collectors.toList());

System.out.println(squaredEvens);  // Output: [4, 16]
```

3) `Functional Interfaces`:

```
@FunctionalInterface
public interface MyFunctionalInterface {
    void myMethod();  // Single abstract method
}

// Using lambda expression to implement functional interface
MyFunctionalInterface myFunc = () -> System.out.println("Functional Programming in Java!");
myFunc.myMethod();  // Output: Functional Programming in Java!
```

4) `Method References`:

```
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

// Using method reference to print each name
names.forEach(System.out::println);  // Output: Alice, Bob, Charlie
```

5) `Default and Static Methods in Interfaces`:

```
interface MyInterface {
    default void printMessage() {
        System.out.println("Default method in interface!");
    }

    static void staticMethod() {
        System.out.println("Static method in interface!");
    }
}

class MyClass implements MyInterface {
    // Inheriting default method
}

MyClass myClass = new MyClass();
myClass.printMessage();  // Output: Default method in interface!
MyInterface.staticMethod();  // Output: Static method in interface!
```

---

**What is Red/Black tree?**

Червено-черното дърво (или `Red-Black tree`) е самобалансирано двоично дърво за търсене, което гарантира, че операциите
за търсене, вмъкване и изтриване могат да бъдат извършвани за време O(log n), където n е броят на елементите в дървото.
Червено-черното дърво се нарича така, защото всяко от върховете му е оцветено в един от два цвята — червен или черен.
Това оцветяване на върховете е основният механизъм за осигуряване на баланс в дървото и предотвратяване на "дълги
вериги", които биха могли да доведат до неефективни операции.

При всяка операция за вмъкване или изтриване, дървото може да наруши тези правила. За да се възстановят правилата, се
използват различни операции за въртене на дървото и преконфигуриране на цветовете на възлите. Тези операции гарантират,
че дървото остава балансирано след всяка операция.

В Java `TreeSet` и `TreeMap` са имплементирани чрез червено-черни дървета, което означава, че тези колекции поддържат
елементи в сортиран ред и извършват операциите за търсене, вмъкване и изтриване за време O(log n).

Преимущества на червено-черното дърво:

- `Логаритмична времева сложност`: Операциите за търсене, вмъкване и изтриване са оптимизирани до O(log n), което
  осигурява ефективност дори при големи количества данни.
- `Самобалансиране`: Червено-черното дърво се грижи за балансирането на структурата, което предотвратява деградиране до
  линейна структура (като списък).
- `Приложимост`: Използва се широко в библиотеки, които изискват ефективно поддържане на сортирани данни, като например
  TreeSet и TreeMap в Java.

Недостатъци:

- `По-сложен код`: В сравнение с други структури от данни, като например хеш таблици, имплементацията на червено-черното
  дърво може да бъде по-сложна.
- `По-висока константна сложност`: Въпреки че времевата сложност е логаритмична, за някои операции константата може да
  бъде по-висока в сравнение с други структури (например хеш таблици).

---

****

## **High-Quality Code**

### Questions that you must be able to answer in detail

- **What is the purpose of unit tests?**
- What other types of testing do you know?
- **What makes a unit test high-quality?**
- **Give some clean code practices examples?**

---

**What is SOLID?**

SOLID is an acronym that represents five principles of object-oriented design that aim to make software more
maintainable, flexible, and easy to understand. Here are the explanations of each of the five SOLID principles, along
with examples:

1) `Single Responsibility Principle (SRP)`: This principle states that a class should have only one reason to change. In
   other words, a class should have only one responsibility or job. For example, if we have a class named User, it
   should only be responsible for handling user-related functionalities such as authentication, user data management,
   etc. It should not be responsible for other unrelated functionalities like sending emails or managing payment
   transactions.
2) `Open/Closed Principle (OCP)`: This principle states that a class should be open for extension but closed for
   modification. This means that we should be able to add new functionalities or behaviors to a class without changing
   its existing code. For example, instead of modifying an existing Payment class to add support for a new payment
   method, we can create a new PaymentMethod class that implements a Payment interface and inject it into the Payment
   class.
3) `Liskov Substitution Principle (LSP)`: This principle states that derived classes should be able to replace their
   base classes without affecting the correctness of the program. In other words, if we have a base class Animal and a
   derived class Dog, we should be able to use the Dog class wherever we use the Animal class. For example, if we have a
   method that takes an Animal parameter and performs some action, we should be able to pass a Dog object to that method
   without any issues.
4) `Interface Segregation Principle (ISP)`: This principle states that a class should not be forced to implement
   interfaces it does not use. In other words, we should separate interfaces that are too large or general into smaller
   and more specific interfaces. For example, instead of having a single Payment interface that includes all payment
   methods, we can have separate interfaces like CreditCardPayment, PayPalPayment, etc.
5) `Dependency Inversion Principle (DIP)`: This principle states that high-level modules should not depend on low-level
   modules. Instead, both should depend on abstractions. This means that we should rely on abstractions, rather than
   concrete implementations. For example, instead of depending on a specific database implementation in a class, we
   should depend on a database interface, which can be implemented by different databases. This allows for easier
   testing, maintenance, and scalability.

---

**Give a real-life example of Open for extension / closed for modification?**

Without OCP (Modifying Existing Code):

```
class Shape {
    public double area(String shapeType, double... dimensions) {
        if (shapeType.equals("Circle")) {
            return Math.PI * dimensions[0] * dimensions[0];
        } else if (shapeType.equals("Square")) {
            return dimensions[0] * dimensions[0];
        }
        return 0;
    }
}
```

If we want to add support for a Rectangle, we have to modify the existing Shape class with additional `else if`.

With OCP (Extending Code, Not Modifying):

```
interface Shape {
    double area();
}

class Circle implements Shape {...}

class Square implements Shape {...}

class Rectangle implements Shape {...}

public class ShapeCalculator {
    public double calculateArea(Shape shape) {
        return shape.area();
    }
}
```

---

**Give a real-life example of Liskov Substitution Principle.**

Let’s consider a scenario with birds. You have a base class `Bird`, and two subclasses `Sparrow` and `Penguin`.
The `Bird` class has a method `fly()`.

```
class Bird {
    public void fly() {
        System.out.println("Flying...");
    }
}

class Sparrow extends Bird {
    @Override
    public void fly() {
        System.out.println("Sparrow is flying...");
    }
}

class Penguin extends Bird {
    @Override
    public void fly() {
        // Penguins can't fly, so we should not have this behavior.
        // This violates LSP!
        throw new UnsupportedOperationException("Penguins can't fly!");
    }
}
```

In this example, the `Penguin` class violates LSP because it can't fly, but it still inherits from `Bird`, which expects
all birds to be able to fly. If we replace a `Bird` object with a `Penguin`, we break the program's behavior
(since `Penguin` can’t fly).

Correct Example (Respecting LSP):

```
interface Bird {
    void move();
}

class Sparrow implements Bird {
    @Override
    public void move() {
        System.out.println("Sparrow is flying...");
    }
}

class Penguin implements Bird {
    @Override
    public void move() {
        System.out.println("Penguin is swimming...");
    }
}
```

Now, both `Sparrow` and `Penguin` implement the `Bird` interface and override the `move()` method in a way that makes
sense for
their specific behavior. You can substitute any `Bird` (whether it's a `Sparrow` or `Penguin`) and the program will
still work
correctly.

---

**Give a real-life example of Dependency Inversion principle.**

Imagine you're building a payment processing system for an online store. You have two classes:

- `PaymentProcessor` (high-level module that coordinates the payment process)
- `CreditCardPayment` (low-level module that actually processes credit card payments)

Without applying the Dependency Inversion Principle, the high-level PaymentProcessor directly depends on a low-level
class like CreditCardPayment:

```
class PaymentProcessor {
    private CreditCardPayment creditCardPayment;

    public PaymentProcessor() {
        this.creditCardPayment = new CreditCardPayment();  // Direct dependency
    }

    public void processPayment(double amount) {
        creditCardPayment.process(amount);  // Directly calling low-level method
    }
}

class CreditCardPayment {
    public void process(double amount) {
        System.out.println("Processing credit card payment of " + amount);
    }
}
```

Correct Example (Respecting the Dependency Inversion Principle):

Now, let's apply the Dependency Inversion Principle by introducing an abstraction (an interface or abstract class) for
payment methods:

```
interface PaymentMethod {
    void process(double amount);  // Abstraction
}

class CreditCardPayment implements PaymentMethod {
    @Override
    public void process(double amount) {
        System.out.println("Processing credit card payment of " + amount);
    }
}

class PayPalPayment implements PaymentMethod {
    @Override
    public void process(double amount) {
        System.out.println("Processing PayPal payment of " + amount);
    }
}

class PaymentProcessor {
    private PaymentMethod paymentMethod;  // High-level module depends on abstraction

    // Dependency Injection (passing the concrete implementation through constructor)
    public PaymentProcessor(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void processPayment(double amount) {
        paymentMethod.process(amount);  // Calls the method of the passed PaymentMethod
    }
}
```

Explanation:

- `Abstraction` (PaymentMethod Interface): The high-level PaymentProcessor class no longer depends on a specific
  low-level
  module (like CreditCardPayment or PayPalPayment). It depends on the PaymentMethod interface, which is an abstraction.
- `Low-Level Modules`: CreditCardPayment and PayPalPayment are now separate classes implementing the PaymentMethod
  interface. The low-level classes depend on the abstraction (PaymentMethod), not on the high-level module.
- `Dependency Injection (DI)`: The actual PaymentMethod (e.g., CreditCardPayment or PayPalPayment) is injected into the
  PaymentProcessor class. This can be done through constructor injection (as shown) or through setters or other methods.

---

#### Questions you should have general knowledge of and should answer in a few words

- How do dependency injection containers work under the hood?
- What is Singleton pattern and how to design it in your class structure?
- What is lazy and eager initialization?

****

## **Database Systems**

### Questions that you must be able to answer in detail

**What is a database server?**

Релационните бази данни са сред най - разпространените. Те предоставят логически групирани данни,
достъпа до съхраняваната информация става бързо и ефикасно. Този тип бази данни имат и механизъм за защите на данните,
което в днешно време е жизнено важно. Сървърът служи за съхранение на информацията.

---

**What is a RDBMS?**

Освен че може да се разглежда информацията която базата съхранява, през RDBMS може да управляваме ограниченията
които се поставят на данните в базата данни. Казано по друг начин RDBMS е софтуер който служи за управление на данните
в базата данни - пример: Oracle, SQL Server, MariaDB и др.

---

**What is the difference between primary and foreign key?**

- Primary key - уникален идентификатор за всеки запис в таблицата.
- Foreign key - Колона от таблицата, която държи първичния ключ като запис от друга таблица.

---

**What makes a database normalized?**

Нормализацията е процес на организиране на данните в резултат на който се изграждат няколко правила:

1. Липса на повторения
2. Ефикастност на операциите върху базата данни
3. Подобряване на консистентността на данните.

---

**What is a constraint and which one do you know?**

Ограниченият очертават правила, които записите в таблицата трябва да спазват

1. Първичния ключ в една таблица е уникален за всеки ред и не се повтаря.
2. Външния ключ се грижи да осъществи връзката в една таблица с друга таблица.
3. Уникалност - ако една колона има това ограничение, всички стойности в тази колона трябва да са уникални.

---

**What is auto increment?**

Auto Increment е функция, която работи с числови типове данни. Той автоматично генерира последователни цифрови
стойности всеки път, когато запис се вмъква в таблица за поле, дефинирано като автоматично увеличаване.

****

## **Database Systems**

### Questions that you must be able to answer in detail

**What is a domain-specific language?**

-Специфичен за домейн език ( DSL ) е компютърен език, специализиран за определен домейн на приложение.
Съществуват голямо разнообразие от DSL, вариращи от широко използвани езици за често срещани домейни,
като HTML за уеб страници, до езици, използвани само от един или няколко софтуерни продукта.

---

**Can SQL be used with another relational database server or is it for MariaDB only?**

Тъй като MariaDB е RDBMS тя използва SQL като език за връзка с базата данни. Други подобни на това приложение
системи също могат да използват SQL език но с различен диалект специфичен за сърварното приложение.

---

**What is a join?**

Тези заявки свързват таблици на базата данни по обща помежду си колона.

---

**How many types of joins do you know?**

Има различни видове JOIN заявки:

1. Inner join - заявка, при която се връща резултат сечение на двете участващи таблици.
2. Left (Outer) join - заявката ще върне всички стойности от лявата таблица, а липсващите такива от дясно ще замени с
   NULL.
3. Right (Outer) join - заявката ще върне всички стойности от дясната таблица, а липсващите от ляво ще замени с NULL.

**What is a union?**

Използва се за да комбинира резултатите от две или повече SELECT заявки във един общ резултат.

По подразбиране ще получим уникални записи, но ако използваме UNION ALL - ще се покажат всички резултати.

````
SELECT * FROM Table1
UNION ALL
SELECT * FROM Table2
````

---

**What is an alias?**

Псевдонимите често се използват, за да направят имената на колоните по-четливи. По подразбиране за да създадем всевдоним
на дадена колона се изпозлва "AS".
Следният SQL израз създава два псевдонима, един за колоната CustomerID и един за колоната CustomerName:

````
SELECT CustomerID AS ID, CustomerName AS Customer
FROM Customers;
````

****

## **Spring and web**

### Questions that you must be able to answer in detail

**What is Spring?**

Spring прави програмирането на Java по-бързо, по-лесно и по-безопасно за всички. Фокусът на Spring върху скоростта,
простотата и производителността я направи най-популярната в света рамка за Java.

---

**What are some of the core modules in Spring?**

---

**What is Java bean?**

В контекста на Spring, Bean е обект, който се създава и управлява от Spring IoC(Inversion of control) контейнера. Това
означава, че Spring контролира жизнения цикъл на Bean-a.

---

**What is Spring IoC**

IoC контейнерът управлява жизнения цикъл на обектите(Bean-новете) в приложението. Вместо сървизите да бъдат инстанцирани
сами, затова се грижи контейнера. В Spring основния компонент за IoC контейнера е `ApplicationContext`. Това е
интерфейс, който разширява BeanFactory и предоставя всички функции за управление на Beam-новете.

---

**What scopes can have Beans?**

Всеки път когато даден Bean бъде инстанциран от Spring, автоматично му се задава и scope.
Типовете на scopes са следните:

1) `Singleton` - Означава, че Spring създава само една инстания на Bean-a, и я преизползва навсякъде през приложението.
   Това означава, че всеки път когато този Bean бива инжектиран някъде, същата инстанция ще бъде използвана.
2) `Prototype`- нова инстанция се създава всеки път, когато bean-ът е повикан(requested).
3) `Request` - Ограничен е до HTTP Request, означава че всеки рекуист получава уникален Bean.
4) `Application` - Ограничен до живота на ServletContext.

---

**What is serialization?**

Сериализацията е преобразуване на състоянието на обект в байтов поток; десериализацията прави обратното.
Казано по различен начин, сериализацията е преобразуването на Java обект в статичен поток (последователност) от байтове,
който след това може да бъде записан в база данни или прехвърлен през мрежа.

---

**What is AOP?**

Аспектно-ориентираното програмиране ( AOP ) е подход към програмирането, който ни позволява да добавяме допълнителна
логика(аспекти) към метод или клас, без да променяме съществуващ код. По този начин ние надраждаме нашата логика, без да
променяме съществуващата такава.
Пример: Анотацията `@Transactional`, Когато маркираме даден метод с тази анотация , казваме на Spring да добави
транзакционна логика към него, без да променяме основния метод.

---

**What is Spring Initializr?**

Spring Initializr е уеб-базиран инструмент, предоставен от Pivotal Web Service. С помощта на Spring Initializr
можем лесно да генерираме структурата на Spring Boot Project. Той предлага разширяем API за създаване на JVM-базирани
проекти.

---

**What is Spring Boot?**

Spring Boot улеснява създаването на самостоятелни, продуктивни приложения, базирани на Spring, които можете да "просто
стартирате".

---

**What does a build tool do?**

Инструментите за изграждане са програми, които автоматизират създаването на изпълними приложения от изходния код
(например .apk за приложение за Android). Build-a включва компилиране, свързване и опаковане на кода в използваема или
изпълнима форма.

---

**What is a REST Controller?**

Spring RestController анотация е удобна анотация, която сама се коментира с @Controller и @ResponseBody.
Тази анотация се прилага към клас, за да го маркира като обработващ заявка.
Анотацията Spring RestController се използва за създаване на RESTful уеб услуги, използвайки Spring MVC.
Spring RestController се грижи за картографиране на данните на заявката към дефинирания метод за обработка на заявки.
След като тялото на отговора се генерира от метода на манипулатора, то го преобразува в JSON или XML отговор.

---

**What is JDBC and Hibernate and what is the difference?**

---

**What is transaction and why do we use it?**

---

**What is HTTP?**

HTTP е абривиатура на HyperText Transfer Protocol, и представлява набор от правила за обмен на ресурси
по интернет. HTTP използва така нареченият клиент сървър модел, като ролята на клиента е да иска ресурси,
а на сървъра да ги връща. Когато отворим една страница, браузърът изпраюа HTTP протокол към сървъра, сървъра обработва
заявката и връща отговор към браузъра, често това е JSON или път HTML страница която трябва да се визуализира.

---

**Which are the main HTTP methods?**

- `GET` - Заявява представяне на даден източник. Заявките, използващи GET би трябвало единствено да извличат данни,
  без да правят каквито и да е промени на сървъра.
- `HEAD` - Заявява същия отговор както и GET, но без тялото на отговора.
- `POST` - Използва се за изпращане единица съдържание към указания източник,
  като по този начин често променя състоянието на източника или предизвиква други странични ефекти на сървъра.
- `PUT` - Замества всички текущи представяния на целевия източник със съдържанието на заявката.
- `DELETE` - Изтрива указаният източник.
- `CONNECT` - Установява тунел към сървъра, указан от целевият източник.
- `OPTIONS` - Използва се, за да се опишат възможностите за общуване с целевият източник.
- `TRACE` - Извършва проверка на пътя за достъп до източника.
- `PATCH` - Използва се, за да приложи частични промени върху даден източник.

---

**Which are the HTTP response status codes?**

Всеки отговор на сървъра съдържа освен Headers и искания ресурс, така и статус код.
Те са разделени на 5 основни групи:

1. 1xx (информативни) - заявката е получена и обработена.
2. 2хх (успешни) - зявката е получена, разбрана и приета.
3. 3хх (пренасочване) - търсеният ресурс е преместен на нов адрес.
4. 4хх (грешка в клиента) - обозначават грешка причинена от клиента.
5. 5хх (грешка в сървъра) - обозначават грешка придизвикана на сървъра.

---

**What is the difference between `HTTP GET` and `HTTP POST`?**

GET заявката се използва за преглед на нещо без то да се променя, докато POST заявката се използва
за промяна на нещо. Например страница за търсене трябва да изпозлва GET за получаване на данни,
докато формуляр, който променя паролата ви, трябва да използва POST заявка.

---

**What is the difference between `HTTP PUT` and `HTTP POST`?**

- Разликата между двете е, че ако изпратим няколко PUT заявки с едно и също съдържание,
  те ще продуцират един и същи резултат. Докато ако изпратим пет POST заявки с едно и съшо съдържание,
  сървърът ще създаде пет еднакви ресурса.

---

**What HTTP method you’d use if you had to get resource from the server, but to do so you need to send lots of data?**

- Бих използвал POST заявката, тъй като тя е предназначена за изпращане на много данни към сървъра
  от определен ресурс.

---

**What is the format of a HTTP request and response?**

- `HTTP request` са съобщения, изпратени от клиента за започване на действие на сървъра.
  Те се сърдържат от 3 елемента:

1. Глагол (GET, PUT или POST) или съществително (HEAD, OPTIONS), който описва действието.
2. Целта на заявката (обикновена е URL)
3. Версията на HTTP, който определя структората на останалите съобщения.
   Типичен HTTP request изглежда по следния начин: `GET /background.png HTTP/1.0`
   или `HEAD /test.html?query=alibaba HTTP/1.1`.

- `HTTP response` - наричан още ред на състоянието, съдържа следната информация:

1. Версия на протокола, обикновено е HTTP/1.1.
2. Код на състоянието, което показва, успех или провал на искането.
3. Състоянието на текста, което представлява инфорамационно съобщение.
   Типичен HTTP response изглежда по следния начин: `HTTP/1.1 404 Not Found.`

---

**What is REST service?**

- REpresentation State Transfer, архитектурен стил, който дефинира стандарт по който системите да си комуникират.
  Това което, ни дава REST е как да бъде Setup-нат сървара така, че клиентите да си комуникират по най-лесния начин.
  Основната причина да съществува този стандарт е да можем да разграничим клиента и сървъра като два отделни компонента.
  Както HTTP така и REST е напълно Stateless, това означава че сървъра няма нужда да знае какво е състоянието на
  клиента,
  и обратното. Отговорът на REST, най-често бива един JSON обект, който се визуализира по определен начин от клиента.
  Ако клиента е мобилно приложение ще бъде по един начин, ако е браузър по друг.

---

**What is API and how can you use it?**

- API или Application Programming Interface, е интерфейс който позволява на две приложения да говорят помежду си.
  Всеки път когато използваме приложение като Facebook, то се свързва с интернет и изпраща данни към сървъра.
  След това сървърът извлича тези данни, интерпретира ги, извършва необходимите действия и ги изпраща обратно на нашия
  телефон.
  След това приложението интерпретира тези данни и ви предоставя информацията, която сте искали, по четлив начин.

---

**What is a RESTful API?**

- Restful е уеб приложение, което използва принципите на HTTP и REST. Представлява колекция от ресурси с четири
  дефинирани аспекта:

1. Основният „URL“ за уеб приложенията.
2. Internet media типът на данните поддържани от уеб приложенията. Това най-често е JSON,
   но може да бъде всеки друг валиден Интернет медиен тип, като се има предвид, че е валиден хипертекст стандарт.
3. Операции поддържани от уеб приложението използвайки HTTP методи (примерно: GET, PUT, POST, или DELETE).
4. Приложенията трябва да се задвижват от хипертекст.

---

**What is the client-server model?**

- Модела клиент-сървър е разпределената структура на приложението, която разделя задачи или натоварвания между
  доставчиците на ресурс или услуга, наречени сървъри, и заявяващи услуги, наречени клиенти.

---

**How are HTTP and REST different?**

- REST е начинът, по който трябва да се използва HTTP. Днес ние използваме само малка част от методите
  на HTTP протокола - а именно GET и POST. Основният начин да го направите е да използвате всички методи на протокола.
  Например REST диктува използването на DELETE за изтриване на данни (било то файл, състояние и т.н.), докато при HTTP
  бихте използвали неправилно GET или POST заявка.

---

**What are HTTP headers and what they are used for?**

- В headers HTTP служат за подаване на допълнителна информация между клиентите и сървъра чрез request и response header.
  Някой от най - често изпозлваните категории за headers са следните:

1. Athorization - Използва се за ауторизация за да се ограничи достъпа до даден ресурс.
2. Caching - Изпозлва се за кеширане на информация която постъпва.
3. Cookies - Данните които се пазаят за даден потребител.
4. Security - Използва се за защите на данните, най - често Log in през някой сайт.

---

**What is it to be "stateless"?**

- Stateless означава че сървара няма нужда да знае какво е състоянието на клиента(т.е. дали request се изпраща от уеб
  браузър,
  десктоп приложение, или мобилно приложение, и обратното клиента няма нужда да знае какво е състоянието на сървара.

---

**What is meant by "business logic"?**

- В едно приложение, бизнес логиката ще се използва за валидация и проверки, така че, да не зависи от база данни.

---

**What is an endpoint?**

- Просто казано, крайна точка е единият край на комуникационния канал. Когато API взаимодейства с друга система,
  допирните точки на тази комуникация се считат за крайни точки. Всяка крайна точка е местоположението, от което
  приложните програмни интерфейси (API) могат да получат достъп до ресурсите, необходими за изпълнението на тяхната
  функция.

---

**Why even bother using REST?**

- Ако трябва да изпозлваме HTTP и неговите методи, при заявка която изтрива ресурс трябва да изпозвале POST заявка.
  докато REST ни позволява да го направим много по лесно (чрез DELETE заявка).
  Някой от предиствата са следните:

1. Просто е
2. Можете да използвате добре HTTP кеша и прокси сървъра, за да ви помогне да се справите с голямо натоварване.
3. Помага ви да организирате дори много сложно приложение в прости ресурси.
4. Улеснява новите клиенти да използват приложението ви, дори ако не сте го проектирали специално за тях
   (вероятно, защото те не са били наоколо, когато сте създали приложението си).

---

**Explain the MVC design pattern?**
-
---

**What is template engine and where do we use it?**
-
---

**What is the difference between dynamic web pages and single page application (SPA)?**
-
---

**What are transactions in MariaDB /Spring?**

- Транзакцията е последователност от операции, третирана като единична единица и неделима (атомна) по време
  на възстановяване на повреда и която изолира между едновременните достъпи в една и съща маса данни.
  Принципно транзакциите или се случват всички или не се изпълнява нито една.

---

### Questions for HTML

**What is HTML?**

- HTML означава Hyper Text Markup Language. HTML е стандартният език за маркиране за създаване на уеб страници.
  Той описва структурата на уеб страница и се състои от поредица от елементи, тези елементите казват на браузъра как да
  показва съдържанието.

---

**What is a browser? How is it different from node.js?**
-
---

**What is semantic HTML? How do we achieve it?**
-
---

### Questions for CSS

**What is CSS? What problems does it solve?**
-
---

**Which are the three ways of inserting CSS into a web page?**
-
---

**What is the difference between ids and classes? When to use what?**
-
---

**Which are the types of selectors you can use?**
-
---

**What is the selector priority hierarchy?**
-
---

**What is the difference between block elements and inline elements?**
-
---

**What is the grid layout?**
-
---

### Questions for layered Architecture and Spring IoC

**Why we want to split our applications into layers?**
-Многослойната архитектура е свързана с организацията на кода за разделяне на проблемите.
Всеки слой съдържа обекти, свързани с определен проблем. Друга причина поради която този архитектурен стил
се е наложил е, че той предоставя естествен начин за разработка на приложения. Тази архитектура спомага
да разкачим и премахнем твърдите връзки на една софтуерна система.

![alt_text](input-images/spring-layers.png)
---

**What is to role of the presentation layer?**

- Presentation слоят се грижи за итеракцията с потребителя, той обработва заявки от потребителя,
  и делигира към бизнес слоя тяхното изпълнение очаквайки отговор който да върне на потребителя.

---

**Where all core application logic belongs?**

- Можем да наречен бизнес слоя, ядрото на приложението, тук живее функционалноста на приложиението което изграждаме.
  Ако извършваме приложение за превод от английски - български примерно, бизнес слоя ще се погрижи да извърши самият
  превод.

---

**What are the benefits of structuring the applications into layers?**

- Една от причините за използване на многослойна архитектура е колко лесно бива разработено дадено приложение.
  Всеки от слоеве може да се разработва независимо от другите.
- Следващият голям плюс е леснотата с която може да се управляват различните слоеве.
- Подобрената възможност за тестване, създадват се юнит тестове върху основната логика на приложението.
- Възможността за преизволзване или с други думи, можем да имаме няколко representation слоя, които да използват една
  и съща бизнес логика (пример: за приложението което ние разработваме можем да имаме web client, мобилно приложение,
  десктоп приложение -
  това ще бъдат 3 различни presentation слоя, но те всички ще работят с един и също бизнес слой.)

---

**What is Dependency Inversion?**

- Похват за премахване на зависимостите на различните софтуерни компоненти, това което този принцип гласи е
  че модулите от високо ниво, не трябва да зависят на модолуте от ниско ниво. Вместо това и двете трябва да зависят на
  абстракции.

---

**What is Inversion of Control?**

- Това е още един принцип в програмирането, при който конфигорирането, създаването и управлението на обекти се делигира
  от някой framework. Следвайки този принцип всеки модул който използваме може да се фокусира изцяло върху това, за
  което
  е създаден. Също така отделните модули нямат твърди връзки помежду си, а зависят на интерфейси.

---

**What is Dependency Injection?**

- Конкретен похват за постигане на Inversion of control. При този похват модулите се разделят на 4 части.

1) Service - модули на които други модули зависят.
2) Client - зависят на модула на Service.
3) Interface - декларират договори за това как различните клиенти могат да достъпват даден Service.
4) Injector - създава Service и го инжектира в клиента.

---

**How many ways are there to inject a dependency?**

- Constructor injection - Това, което е специфично за това метод е, че модула който има някаква зависимост,
  получава инстанция на тази зависимост през конструктора.

````
    BeerServiceImpl(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }
````

- Setter injection - Модула който има зависимост, предоставя setter при който инстанция на зависимостта
  да бъде инжектирана.

````
    public void setBeerRepository(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }
````

---

**Explain the difference between Dependency Inversion, Inversion of Control & Dependency Injection.**

- Основната цел е да се създаде приложение, чиято подръжка е възможно най-лесна.
  За тази цел е важно да се мисли предварително как отделните модолу си комуникират помежду си.
  За осъществане на DIP се прилага IoC, който измества отговорността за създаване и управляване
  на зависимостите в едно централно място (IoC Conteiner). Използваме DI който инжектира зависимостите до местата
  където те са нужни.

---

**What are the benefits of DI?**

- Late binding - възможността да подменяме имплементациите на зависимости, докато програмата се изпълнява
  без да се налага да я рестартираме.
- Extensibility - По - лесно надграждане и разширяване на приложението.
- Parallel development - Два екипа могат да работят по различни части на приложението без да си пречат.
- Maintainabillity, Testability - Възможност за подръжка и тестваемост са може би сред най-ценните плюсове на
  инжектирането.

---

#### Questions you should have general knowledge of and should answer in a few words

- What is the role of “enabled” column in Spring Security Users table?
- What is Aspect Oriented Programming (AOP)?
- How does Spring Security work under the hood?
- What is database migration?

****

## **Practical Tasks**

**Write function to print Fibonacci numbers up to N.**

````
    private static int fib(int n) {
        if (n <= 1) {
            return n;
        }

        return fib(n - 1) + fib(n - 2);
    }
````

**Write function to find substring in string.**

````

````

**Write a function that takes a String and reverses it?**

````
    public static String reverseString(String str) {
        if (str.isEmpty()) {
            return str;
        }

        if (str.length() == 1) {
            return str;
        }

        return reverseString(str.substring(1)) + str.charAt(0);
    }
````

````
    public static String reverse(String input) {

        if (input == null) {
            return input;
        }

        StringBuilder output = new StringBuilder();

        for (int i = input.length() - 1; i >= 0; i--) {
            output.append(input.charAt(i));
        }

        return output.toString();
    }
````

**Write a function to check if a String is a palindrome or not?**

````
    public static boolean isPal(String s) {
        if (s.length() == 0 || s.length() == 1)
            return true;
        if (s.charAt(0) == s.charAt(s.length() - 1))
            return isPal(s.substring(1, s.length() - 1));

        return false;
    }
````

**Write a function to check if two Strings are anagrams in Java?**

````
    public static boolean checkAnagram(String first, String second) {
        char[] characters = first.toCharArray();
        StringBuilder sbSecond = new StringBuilder(second);
        for (char ch : characters) {
            int index = sbSecond.indexOf("" + ch);
            if (index != -1) {
                sbSecond.deleteCharAt(index);
            } else {
                return false;
            }
        }
        return sbSecond.length() == 0;
    }
````

**Write function to print all Character permutations in given string.**

````
    private static void permutation(String prefix, String str) {
        int n = str.length();
        if (n == 0) System.out.println(prefix);
        else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n));
        }
    }
````

**Write a function to count the number of occurrences of a given Character in a String?**

````
int occurance = StringUtils.countOccurrencesOf("a.b.c.d", ".");
````

````
public static int countOccurrences(String haystack, char needle) {
    int count = 0;
    for (int i=0; i < haystack.length(); i++) {
        if (haystack.charAt(i) == needle) {
             count++;
        }
    }
    return count;
}
````

**Which data structure you’d use to check whether two strings are palindromes?**

````
1) Loop
2) Stack
3) Recursive
````

**What is the fastest way of mirroring a number? 12 -> 21**

````
    private static int reverseNum(int num) {
        StringBuffer string = new StringBuffer(String.valueOf(num));

        string.reverse();

        num = Integer.parseInt(String.valueOf(string));
        
        return num;
    }
````

**Write function to find a number in binary search tree.**

````
private static boolean ifNodeExists( Node node, int key) {
    if (node == null)
        return false;
 
    if (node.data == key)
        return true;
 
    boolean res1 = ifNodeExists(node.left, key);
   
    if(res1) return true;
 
    boolean res2 = ifNodeExists(node.right, key);
 
    return res2;
}
````

**Write function to convert entity to standard DTO obeject.**

````
  public Function<NotificationEntity, Notificatio> convertToNotification() {
	return entity -> new Notificatio(entity.id(), entity.status(), entity.process(), entity.stage());
  }
  
  And then when we need to use it in List operation or map ->
  return notifications.map(service.convertToNotification()).toList();
````

**Given a string containing just the characters `(`, `)`, `{`, `}`, `[` and `]`, determine if the input string is valid.
**
An input string is valid if:

- Open brackets must be closed in the correct order.
- Open brackets must be closed by the same type of brackets.

````
    private static boolean isValid(String input) {
        Stack<Character> stack = new Stack<>();
        for (char c : input.toCharArray()) {
            if (c == '(') stack.push(')');
            else if (c == '[') stack.push(']');
            else if (c == '{') stack.push('}');
            else if (stack.isEmpty() || stack.pop() != c)
                return false;
        }
        return stack.isEmpty();
    }
````

**Write function `mergeStrings("abc", "defg")` ... = `"adbecfg"`**

````

````

**You have defined “weight” of a string. Get the lightest string with this “weight”.**

````

````

**You have several strings. How would you check whether each of them contains all letters from the alphabet?**

````

````

**Find the count of unique numbers in an array. `[1,3,3,4,4,4] -> 2`**

````
    public static int diffValues(int[] numArray) {
        int numOfDifferentVals = 0;

        List<Integer> diffNum = new ArrayList<>();

        for (int i = 0; i < numArray.length; i++) {
            if (!diffNum.contains(numArray[i])) {
                diffNum.add(numArray[i]);
            }
        }

        if (diffNum.size() == 1) {
            numOfDifferentVals = 0;
        } else {
            numOfDifferentVals = diffNum.size();
        }

        return numOfDifferentVals;
    }
````

## **Logical Tasks**

- You are given two eggs, and access to a 100-storey building. Both eggs are identical. The aim is to find out the
  highest floor from which an egg will not break when dropped out of a window from that floor. If an egg is dropped and
  does not break, it is undamaged and can be dropped again. However, once an egg is broken, that’s it for that egg. If
  an egg breaks when dropped from floor n, then it would also have broken from any floor above that. If an egg survives
  a fall, then it will survive any fall shorter than that. The question is: What strategy should you adopt to minimize
  the number egg drops it takes to find the solution?. (And what is the worst case for the number of drops it will
  take?)

    - Ако стъпката ни е 10, почваме на 10-ти етаж, хвърляме и то не се чупи. Отиваме на 20-ти, пак не се чупи.
      Отиваме на 30-ти етаж и яйцето се чупи. Тогава за второто яйце ще имаме само 10 стъпки да минем (от 20 до 30).

    - Започвайки от 14-ти етаж в случая е най-оптималното решение.
      x + (x - 1) + (x - 2) + (x -3) .... = x * (x + 1) /2 = 100
  
  
  
  