# Java Testing Best Practices

We look at some best practices for writing Java effective Integration and Unit tests that catch regressions and
keep code quality high.

## Table of Contents

1. [Integration Tests](#integration-tests)
    - [Integration Tests for `web-flux` Controllers.](#integration-tests-for-web-flux-controllers)
    - [Integration tests for `web` Controllers.](#integration-tests-for-web-controllers)
    - [Integration tests for `rabbit-streams` queues.](#integration-tests-for-rabbit-streams-queues)
    - [Class for access repository `TestDataAccess`.](#class-for-access-repository-testdataaccess)
    - [Jpa test over paginated operation.](#jpa-integration-test-for-paginated-repository)
2. [Unit Tests](#unit-tests)
    - [Usage of `@Captor` in unit tests.](#captor-usage-in-unit-tests)
3. [Testing Tools](#testing-tools)
4. [Good things to know](#good-things-to-know)

## **Integration tests**

Когато пишем интеграционни тестове е важно да знаем, че трябва да се тества целият последващ флоу от операции.

### Integration tests for `web-flux` controllers.

Няколко важни аспекта, преди да продължим с писането на тестовете. Знаем, че когато се пишат интеграционни тестове,
трябва да използваме свързващ компонент като база данни или Rabbit, Kafka и т.н.
Затова тука ще добавим тези интерфейси, които ще бъдат отговорни за създаването на докер контейнери.

````
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public interface DatabaseContainer {

  @Container
  PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(DockerImageName.parse("postgres:15-alpine")
    .asCompatibleSubstituteFor("postgres"));

  @DynamicPropertySource
  static void setProperties(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.hostname", postgres::getHost);
    registry.add("spring.datasource.port", () -> postgres.getMappedPort(5432));
    registry.add("spring.datasource.username", postgres::getUsername);
    registry.add("spring.datasource.password", postgres::getPassword);
    registry.add("spring.datasource.url", postgres::getJdbcUrl);
  }
}
````

````
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public interface RabbitMQContainer {

  @Container
  org.testcontainers.containers.RabbitMQContainer rabbitMQ = new org.testcontainers.containers.RabbitMQContainer(
    DockerImageName.parse("rabbitmq:3")
      .asCompatibleSubstituteFor("rabbitmq"))
    .withVhost("mono")
    .withExposedPorts(5672, 15672);

  @DynamicPropertySource
  static void setProperties(DynamicPropertyRegistry registry) {
    registry.add("spring.rabbitmq.host", rabbitMQ::getHost);
    registry.add("spring.rabbitmq.port", () -> rabbitMQ.getMappedPort(5672));
  }
}
````

Когато пишем тестове за `web-flux` контролери е важно да отбележим, че те се тестват посредством WebClient.
Заюото когато правим заявки, във имплементацията ние ще използваме WebClient.

Пример №1.
Във първия пример ще предположим, че сървиза ни с който ще работим има метод `save`, който ще бъде отговорен за
запазване на данни във нашата база.
Тука метода `generateModel()` е статичен такъв и ще бъде създаден във Fixtures клас, който ще е отговорен за генериране
на нашите обекти.

Чрез използването на метода `service.save(model)` ние запазваме нашите данни в базата и можем да работим с тях след
това.

````
@SpringBootTest
@AutoConfiguWebTestClient
public class FluxControllerTest implements DatabaseContainer {

    @Autowired
    private WebTestClient webClient;
    
    @Autowired
    private OperationService service;
    
    @TestConfiguration
    @ComponentScan(basePackageClasses = OperationService.class)
    public static class TestConfig { }
    
    private final Model model = generateModel();
    
    @Test
    void testGetResultFromControllerWithPathVariableForSingleObject() {
        service.save(model)
        
        final var result = webClient.get()
          .uri("/api/v1/operation/{operationId}", model.operationId())
          .exchange()
          .expectStatus().isOk()
          .expectBody(com.local.model.Model.class)
          .returnResult().getResponseBody();
          
        assertThat(result).isNotNull();
    }

    @Test
    void testGetResultFromControllerWithRequestParamsForSingleObject() {
        service.save(model)
        
        final var result = webClient.get()
          .uri("/api/v1/operation?companyId={companyId}&index={index}",
           model.companyId(), model.index().name())
          .exchange()
          .expectStatus().isOk()
          .expectBody(com.local.model.Model.class)
          .returnResult().getResponseBody();
          
        assertThat(result).isNotNull();
        assertThat(result.companyId()).usingRecursiveComparison().isEqualTo(2);
    }
    
    @Test
    void testGetResultFromControllerWithRequestParamsForLists() {
        service.save(model)
        
        final var result = webClient.get()
          .uri("/api/v1/operation?companyId={companyId}&index={index}",
           model.companyId(), model.index().name())
          .exchange()
          .expectStatus().isOk()
          .expectBody(new ParameterizedTypeReference<List<Integer>>() { })
          .returnResult().getResponseBody();
          
        assertThat(result).isNotEmpty();
        assertThat(result).usingRecursiveComparison().ignoreFields("id").isEqualTo(expectedResult);
    }
    
    @Test
    void testPostSearchResultFromControllerWithRequestBodyForLists() {
        service.save(model)
        
        final var result = webClient.post()
          .uri(uriBuilder -> uriBuilder.path("/api/v1/operations").build())
          .contentType(MediaType.APPLICATION_JSON)
          .bodyValue(searchObject)
          .exchange()
          .expectStatus().isOk()
          .expectBodyList(OpeartionsPage.class)
          .returnResult().getResponseBody();
          
        assertThat(result).usingRecursiveFieldByFieldElementComparatorIgnoringFields("opeartion.id").isNotNull();
    }
}
````

---

### Integration tests for `web` controllers.

Няколко важни аспекта, преди да продължим с писането на тестовете. Знаем, че когато се пишат интеграционни тестове,
трябва да използваме свързващ компонент като база данни или Rabbit, Kafka и т.н.
Затова тука ще добавим тези интерфейсите, посочени в предната точка отново.

Когато пишем тестове за `web` контролери е важно да отбележим, че те се тестват посредством MockMvc.
Заюото когато правим заявки, във имплементацията ние ще използваме RestTemplate.

Пример №1.
Принципа на създадване на обекти тука е същия, ако в сървиза ни съществува метод `save` или `create` - го използваме.
Ако обаче няма такъв, отново трябва да си създадем клас `TestDataAccess`, който ще имплементира репозиторите и ше
генерира
обектите.

Във посочения случай по-долу. `@AutoConfigureMockMvc(addFilters = false)` - означава че тези тестове няма да се влияят
от
spring-security. С две думи - изключваме защитата.

````
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith({SpringExtension.class, MockitoExtension.class})
public class RestControllerTest implements DatabaseContainer {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private OperationService service;
    
    @Autowired
    private ObjectMapper mapper;
    
    private final Operation operation = generateOpeartion();
    
    @Test
    void testGetOperationsWithRequestParams() throws Exception {
        service.save(operation)
        
        final var result = mockMvc.perform(get("/api/v3/operations")
          .param("pageNumber", "1")
          .param("pageSize", "10")
          .contentType(MediaType.APPLICATION_JSON)
          .andExpect(status().isOk())
          .andReturn();
          
        String content = result.getResponse().getContentAsString();
        Operation response = mapper.readValue(content, Operation.class);
        
        assertThat(response.getTotalElements(), greaterThan(20));
        assertThat(response.getTotalPages(), greaterThan(2));
    }

    @Test
    void testGetOperationsWithRequestBody() throws Exception {
        service.save(operation)
        
        final var result = mockMvc.perform(post("/api/v3/operations")
          .content(mapper.writeValueAsBytes(new OperationSearch(params....)))
          .contentType(MediaType.APPLICATION_JSON)
          .andExpect(status().isOk())
          .andReturn();
          
        String content = result.getResponse().getContentAsString();
        List<Operation> response = mapper.readValue(content, new TypeReference<>() {});
        
        assertThat(response).isEqualTo(OPERATIONS_PAGE);
    }
}
````

---

### Integration tests for `rabbit-streams` queues.

Когато пишем тестове за Consumers или Producers е важно да отбележим, че трябва да използваме `RabbitMQContainer`,
тъй като ни е нужно да генерираме тестова среда за `rabbit`. След което, когато започваме писането на тестовете е важно
да се отбележи това, че трябва да добавяме към `TestConfiguratin` всички нужни конфигурационни файлове за Consumer-ите.

```
@StreamRabbitTest
@SpringBootTest(classes = QueueListenersTest.Conf.class)
class QueueListenersTest implements RabbitMQContainer {

  @Import({
    ObjectMapperConfiguration.class,
    QueueOneConfiguration.class,
    QueueTwoConfiguration.class,
    QueueThreeConfiguration.class
  })
  @Configuration
  static class Conf { }

  @MockBean
  OperationService operationService;

  @MockBean
  ConfigurationService configurationService;

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @Autowired
  ObjectMapper objectMapper;

  private final LocalDate today = LocalDate.now();
  private final Group group = new Group(randomInt(), randomString(), new Country(randomInt(), randomString()));
  private final Type type = randomEnum(Type.class);
  private final PeriodTime period = new Period(randomInt(), randomString(), type, today.minusDays(10), today);
  private final Job job = new Job(group, type);
  private final ModelResult modelResult = generateModelResult();
  private MonitoredJob monitoredJob;

  @ParameterizedTest
  @EnumSource(value = ModificationStatus.class)
  void modelStatusModificationStatusTest(final ModificationStatus status) {
    prepare(new ModelAttribute(false, null), status.equals(ModificationStatus.CREATED) ?
      START_JOB : Model.Status.IN_PROGRESS);

    final ModificationStatus modification = new ModificationStatus(group, period, status);
    sendMessage(modification, "model.modified", "#");
  }

  @ParameterizedTest
  @EnumSource(value = Status.class)
  void trainingStatusNotificationTest(final Status status) {
    final boolean available = status == Status.SUCCESS;
    prepare(new ModelAttribute(available, available ? today : null), available ? IN_REVIEW : START_JOB);
    final TrainingStatusNotification notification = new TrainingStatusNotification(job, status);
    sendMessage(notification, "trainer.status", "#");

    await().atMost(10, TimeUnit.SECONDS).untilAsserted(() -> {
      if (available) {
        Mockito.verify(operationService).changeStatus(monitoredJob.job(), monitoredJob.trainer().available(), IN_REVIEW);
      } else {
        Mockito.verifyNoInteractions(operationService);
      }
    });
  }

  @Test
  void modelResultNotification() {
    sendMessage(modelResult, "model.processed", "success");

    await().atMost(5, TimeUnit.SECONDS).untilAsserted(() -> Mockito.verify(configurationService)
      .save(new Job(modelResult.group(), modelResult.period().type()), modelResult.period().id()));
  }

  void prepare(final ModelAttribute attribute, final Model.Status status) {
    monitoredJob = new MonitoredJob(new Job(group, type), attribute, status);
  }

  @SneakyThrows
  private <T> void sendMessage(final T body, final String exchange, String routingKey) {
    var message = MessageBuilder.withBody(objectMapper.writeValueAsBytes(body))
      .andProperties(MessagePropertiesBuilder.newInstance().setContentType("application/json").build()).build();

    rabbitTemplate.convertAndSend(exchange, routingKey, message);
  }
}
```

---

### Class for access repository `TestDataAccess`.

Разгледахме сценариите в които имаме метод `save` във нашия сървиз. Обаче ако нямаме такъв метод и само черпи информация
от базата, тогава какво трябва да нарпавим за да тестваме нашите класове.
Отговора е прост - трябва да създаден нов клас, който вътре ще съдържа репозиторито като дипендънси - `TestDataAccess`.

````
public class TestDataAccess {

    private final OperationRepostiory operationRepository;
    
    @Autowired
    public TestDataAccess(final OperationRepostiory operationRepository) {
        this.operationRepository = operationRepository;
    }
    
    public void saveOperation(Operation operation) {
        operationRepository.save(EntityMapper.toOperationEntity(operation));
    }
}
````

След като създадохме нашия тестови клас, можем да го инжектнем където пожелаем в тестовите сценарии.
Важно е тука да се отбележи това, че задължително трябва да създадем Configuration (`TestConfiguration`) клас, който
ще вкара всичките зависимости.
Отделно от това виждаме че вътре има метод `createEntities`, който в нашия случай просто е отговорен за създаването на
500 отделни `Operation` обекта в базата.

````
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SomeTestClass implements DatabaseContainer {

  @TestConfiguration
  @Import(TestDataAccess.class)
  @ComponentScan(basePackageClasses = {OperationService.class, ProductGroupService.class})
  public static class Configuration {

    @Bean
    List<Operations> createEntities(TestDataAccess testDataAccess, AnotherService anotherService) {
      return IntStream.range(0, 500).mapToObj(value -> generateOperation())
        .peek(operation -> anotherService.saveObject(operation().object()))
        .peek(testDataAccess::saveOperation).toList();
    }
  }

    @Autowired
    private TestDataAccess TestDataAccess;
    
    
    private Operation generateOperation() {
    return new Operation(randomInt(), randomLong(), randomInt(), randomBoolean(), randomEnum(Status.class));
}
````

---

### JPA integration test for paginated repository.

Когато имаме нужда да тестваме нашият сървиз дали извършва правилно операцията, но заедно с това и дали Query-то,
което се намира във репозитори метода си върши работата е нужно да създадем интегратиционен тест върху този сървиз.

1. Обозначаваме нашите тестове като `@DataJpaTest`
2. Създаваме `@Bean`, който всъщност ще повика метода `createExclusion` и ще създаде 100 записа чрез него, чрез метода
в нашия сървиз - `exclusionService.create`
3. Добавяме зависимостите, в случая `@Autowired` са само тези които ще използваме в текущия тест, останалите са просто
`@MockBean`.
4. Пишем всички тестове свързани със това как работи Paginated-операцията.


````
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ExclusionSearchServiceTest implements DatabaseContainer {

  @TestConfiguration
  @ComponentScan(basePackageClasses = ExclusionService.class)
  public static class Configuration {

    @Bean
    List<Exclusion> expectedExclusions(ExclusionService exclusionService) {
      return IntStream.range(0, 100)
        .mapToObj(value -> createExclusion())
        .map(exclusionService::create)
        .toList();
    }
  }

  @Autowired
  private List<Exclusion> expectedExclusions;

  @Autowired
  private ExclusionSearchServiceImpl exclusionSearchService;

  @MockBean
  private ETagExclusionCacheService eTagExclusionCacheService;

  @MockBean
  private ProductGroupService productGroupService;


  @Test
  void getAllExclusions() {
    final Collection<Exclusion> exclusions = exclusionSearchService.getAllExclusions();
    
    assertThat(exclusions).isNotEmpty();
    assertThat(expectedExclusions)
      .usingRecursiveFieldByFieldElementComparatorIgnoringFields("created")
      .containsAnyElementsOf(exclusions);
    assertThat(exclusions).size().isEqualTo(100);
  }

  @Test
  void getFilteredExclusionsByCountryId() {
    final var countryId = expectedExclusions.get(randomSmallInt()).country().id();
    final var filter = new ExclusionFilter(countryId, null);
    filter.setPageNumber(1);
    filter.setPageSize(10);

    final var exclusionsPage = exclusionSearchService.getFilteredExclusions(filter);
    
    assertThat(exclusionsPage).isNotNull();
    assertThat(exclusionsPage.content())
      .usingRecursiveFieldByFieldElementComparatorIgnoringFields("created")
      .containsOnly(
        expectedExclusions.stream()
          .filter(exclusion -> exclusion.country().id() == countryId)
          .toArray(Exclusion[]::new)
      );
  }

  @Test
  void getFilteredExclusionsByCountryIdAndSearchParamShopId() {
    final var randomInt = randomSmallInt();
    final var countryId = expectedExclusions.get(randomInt).country().id();
    final var shopId = expectedExclusions.get(randomInt).shops().stream().findFirst().orElse(null);
    final var filter = new ExclusionFilter(countryId, String.valueOf(shopId));
    filter.setPageNumber(1);
    filter.setPageSize(10);

    final var exclusionsPage = exclusionSearchService.getFilteredExclusions(filter);

    assertThat(exclusionsPage).isNotNull();
    assertThat(exclusionsPage.content())
      .usingRecursiveFieldByFieldElementComparatorIgnoringFields("created")
      .containsOnly(
        expectedExclusions.stream()
          .filter(exclusion -> exclusion.shops().contains(shopId))
          .toArray(Exclusion[]::new)
      );
  }

  @Test
  void getFilteredExclusionsByCountryIdAndSearchParamCompanyName() {
    final var randomInt = randomSmallInt();
    final var countryId = expectedExclusions.get(randomInt).country().id();
    final var companyName = expectedExclusions.get(randomInt).company().name();
    final var filter = new ExclusionFilter(countryId, companyName);
    filter.setPageNumber(1);
    filter.setPageSize(10);

    final var exclusionsPage = exclusionSearchService.getFilteredExclusions(filter);
    
    assertThat(exclusionsPage).isNotNull();
    assertThat(exclusionsPage.content())
      .usingRecursiveFieldByFieldElementComparatorIgnoringFields("created")
      .containsOnly(
        expectedExclusions.stream()
          .filter(exclusion -> exclusion.company().name().equals(companyName))
          .toArray(Exclusion[]::new)
      );
  }

  @Test
  void getCompaniesByCriteriaTest() {
    final var randomInt = randomSmallInt();
    final var countryId = expectedExclusions.get(randomInt).country().id();
    final var companyName = expectedExclusions.get(randomInt).company().name();

    final var result = exclusionSearchService.getCompaniesByCriteria(countryId, companyName);

    assertThat(result).isNotEmpty();
    assertThat(result)
      .containsOnly(
        expectedExclusions.stream()
          .map(Exclusion::company)
          .filter(company -> company.name().equals(companyName))
          .toArray(Company[]::new)
      );
  }

  public static ExclusionCreate createExclusion() {
    return new ExclusionCreate(
      generateCountry(),
      generateCompany(),
      randomString(),
      randomBoolean() ? List.of(randomInt(), randomInt()) : List.of(),
      List.of(randomLong(), randomLong())
    );
  }
}

````

---

### `@Captor` usage in unit tests

`ArgumentCaptor` в Mockito е мощен инструмент, който се използва за улавяне на аргумента, подаден на мокнат метод 
по време на изпълнението на теста. Това ви позволява да проверите реалните стойности на аргументите, 
които са били подадени на метода, за да направите допълнителни проверки.

Как работи?

Първо, дефинирате `ArgumentCaptor` за типа на обекта, който искате да улавяте.
След това, при изпълнението на теста, `capture()` улавя аргументите, подадени на мокнатия метод.
След като тестът се изпълни, можете да вземете уловените стойности и да ги проверите чрез `getValue()` или `getAllValues()`.

Позволява ви да проверите дали даден метод е бил извикан с правилните аргументи.
Особено полезен при методи с `void` изпълнение.

```
@Captor
private ArgumentCaptor<ExclusionEntity> captorExclusion;
```

Този ред дефинира ArgumentCaptor за ExclusionEntity, което означава, че искаме да улавяме екземпляри от този клас,
които се подават на метода save() в exclusionRepository.

```
@ExtendWith(MockitoExtension.class)
class ExclusionServiceTest {

  @Mock
  private ETagExclusionCacheService eTagExclusionCacheService;

  @Mock
  private ExclusionRepository exclusionRepository;

  @Captor
  private ArgumentCaptor<ExclusionEntity> captorExclusion;

  @InjectMocks
  private ExclusionServiceImpl exclusionService;
  
   @Test
   void createExclusion() {
      when(productGroupService.getProductGroups(anyList())).thenReturn(List.of(DEFAULT_PRODUCT_GROUP));
      exclusionService.create(exclusionCreate);

      verify(exclusionRepository, times(2)).save(captorExclusion.capture());

      assertThat(captorExclusion.getValue())
         .usingRecursiveComparison()
         .ignoringFields("id", "created", "country.created", "excludedProductGroups", "shops")
         .isEqualTo(exclusionEntity);
   }
 }
```

Как работи ArgumentCaptor тук?

1. Тестваш create(exclusionCreate), което вътрешно извиква exclusionRepository.save(exclusionEntity).
2. save(exclusionEntity) няма return, така че не можеш директно да провериш резултата.
3. За да провериш дали save() е извикан с правилния обект, използваш ArgumentCaptor.
4. verify(exclusionRepository, times(2)).save(captorExclusion.capture()); улавя аргументите, с които save() е бил извикан.
5. captorExclusion.getValue() ти дава реалния обект, който save() е получил.
6. След това можеш да сравниш този обект с очакваното exclusionEntity.

---

## **Good things to know**

**How to compare two objects in Java without comparing their addresses**

Когато правим някакво сравнение на обекти във нашите Unit тестове, много често не искаме да сравняваме тяхните адреси, а
само пропъртита които имат вътре в тях.
Затова използваме `assertThat(от org.assertj.core.api)` -> това ни дава привилегията, когато сравняваме обектите в Java,
да не гледаме тяхните адреси - за тази цел иползваме `usingRecursiveComparison()`

```
Expected: com.local.model.internal.OperationEntity@61c9206b
Actual: com.local.model.internal.OperationEntity@77b6393

    assertThat(result.operation())
     .usingRecursiveComparison()
     .isEqualTo(operation);
```