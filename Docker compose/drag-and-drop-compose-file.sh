#!/bin/bash

# Function to prompt the user for a file
prompt_for_file() {
  echo "Please drag and drop a valid docker-compose YAML file onto this script."
  read -p "Enter the path to the YAML file: " DOCKER_COMPOSE_FILE
}

# Check if a file was passed as an argument
if [[ -n "$1" && "$1" == *.yaml ]]; then
  DOCKER_COMPOSE_FILE="$1"
else
  # If no valid argument, prompt for a file
  prompt_for_file
fi

# Check if the file is a valid YAML file
if [[ -z "$DOCKER_COMPOSE_FILE" || ! "$DOCKER_COMPOSE_FILE" == *.yaml ]]; then
  echo "Invalid file. Please provide a valid docker-compose YAML file."
  exit 1
fi

# Extract the base name of the YAML file (without extension)
BASE_NAME=$(basename "$DOCKER_COMPOSE_FILE" .yaml)

# Notify the user about starting the services
echo "Starting Docker Compose using: $DOCKER_COMPOSE_FILE"

# Run Docker Compose with the specified compose file
docker-compose -f "$DOCKER_COMPOSE_FILE" up -d

# Notify the user that the process is done
echo "Docker Compose and additional container have been started."