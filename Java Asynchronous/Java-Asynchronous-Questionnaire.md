# Java Asynchronous Best Practices

### Questions that you must be able to answer in detail

**What is difference `@Async` vs. CompletableFuture?**

Реално няма разлика между двете - те са почти едни еднакви технологии.

- `CompletableFuture` предоставя удобен начин да управляваш асинхронизацията през процеса, тоест можеш да викаш различни
  команди от типа на runAsync() или supplyAsync().
- `@Async` предоставя удобен начин да контролираш задачките си, без да се налага да мислиш за имплементацията зад него.
  Ако трябва да използваш @Async, трябва в конфигурационните ти файл да добавиш `@EnableAsync` и да създадеш @Bean
  от `Executor`.

```
@Async
public String compute() {
    // do something really slow
    return "my result"
}
```

Какво би се случило в горе описания метод? Без значение колко бавен би бил метода, при извикването му "my result" ще
бъде
върнат веднага, а това което е абстракно и би се случило зад сцените ще бъде обработката на бавния метод, без това да
блокира текущата нишка.
Горния метод е еквивалент на `supplyAsync()`, ако изпозлваме CompletableFuture.

```
@Bean("executor")
Executor asyncExecutor() { return Executors.newFixedThreadPool(10); }

@Autowire
private Executor executor;

public CompletableFuture<String> computeAsync() {
    return CompletableFuture.supplyAsync(() -> {
        // do something slow
        return "my result";
    }, executor);
}
```

---

**What is difference CompletableFuture.supplyAsync() vs. CompletableFuture.runAsync()?**

1) `supplyAsync()`: This method is used when you need to return a value from the asynchronous computation. It returns a
   `CompletableFuture<T>`, where `T` is the type of the value returned by the computation.
   Главно с използва когато искаме да продуцираме някакъв резултат от асинхронната работа на метода.

```
CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
    // simulate a long-running task
    return 10 * 2; // Returns the computed value
});

// Get the result
future.thenAccept(result -> {
    System.out.println("Result: " + result); // Output: Result: 20
});
```

2) `runAsync()`: This method is used for asynchronous execution of a task that does not return a result. It returns
   a `CompletableFuture<Void>`, indicating that there is no meaningful result returned by the computation.
   Главно с използва когато искаме да изпълним някоя задача на заден план, без да използваме резултата от нея.

```
CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
    // simulate a long-running task
    System.out.println("Task is running");
});

// Wait for the completion of the task
future.thenRun(() -> {
    System.out.println("Task completed");
});
```

---

**In what cases do we use `thenAccept`?**

Метод във CompletableFuture, който ни позволява да обработим резултата след асинрхонната работа на метода.
Осбено важно е че се използва когато искаме да приложим някакви действия върху резултата, но няма да връщаме никакви
данни след това.

```
CompletableFuture<Void> allProcessing = CompletableFuture.allOf(partialRecords.stream()
      .map(record -> CompletableFuture.supplyAsync(() ->
          webClient.getTexts(productGroup.id(), record.companyId(), period.periodStr(), record.objectId()), asyncExecutor)
        .thenAccept(texts -> {
          if (texts.isEmpty()) return;
          storeService.updateDetails(...);
        }))
      .toArray(CompletableFuture[]::new));

    allProcessing.join();
```

Какво би се случило в гореописания метод? Разгледайки сценария, в който имаме три записа в partialRecords, можем да
разгледаме как ще работят нишките и какво ще се случи във `thenAccept`.

- Запис 1: webClient.getTexts връща списък с 2000 записа.
- Запис 2: webClient.getTexts връща списък с 10000 записа.
- Запис 3: webClient.getTexts връща празен списък.

Обработка във `thenAccept`

След приключване на извикванията, всяка нишка ще премине през блока thenAccept за съответния запис:

```
    Запис 1:
        Вход в thenAccept с резултат от 2000 записа.
        Проверка на if (texts.isEmpty()): условието е false.
        Извикване на collectFields(...) за извличане на данни от 2000 записа.
        Обновяване на записа чрез storeService.updateDetails(...).
```

```
    Запис 2:
        Вход в thenAccept с резултат от 10000 записа.
        Проверка на if (texts.isEmpty()): условието е false.
        Извикване на collectFields(...) за извличане на данни от 10000 записа.
        Обновяване на записа чрез storeService.updateDetails(...).
```

```
    Запис 3:
        Вход в thenAccept с празен списък.
        Проверка на if (texts.isEmpty()): условието е true.
        Блокът thenAccept завършва с return, и updateDetails не се извиква.
```

Брой на Нишките

Броят на нишките, които обработват информацията, зависи от конфигурацията на asyncExecutor (например, максималния брой
нишки в пула). В нашия сценарий:

```
    Най-малко три нишки (една за всяко извикване на getTexts) ще бъдат използвани, но е възможно да се използват и повече, в зависимост от наличността на нишки в пула.
    След завършване на асинхронните извиквания, всяка нишка, която е обработила getTexts, ще предаде резултата на thenAccept.
```

---

**What does it mean `.join()` at then of CompletableFuture<Void>?**

Calling `.join()` on a CompletableFuture<Void> ensures that all the asynchronous tasks in the computation pipeline have
completed before the program continues to the next operation. Here's how it works in practice:

1) Waiting for Completion: `.join()` blocks the main thread (or the thread calling `.join()`)
   until the CompletableFuture it’s called on has completed. If the CompletableFuture was created
   using `CompletableFuture.allOf(...)` (to wait for multiple asynchronous tasks), .`join()` ensures that all those
   tasks are complete.

```
CompletableFuture<Void> allTasks = CompletableFuture.allOf(
    CompletableFuture.runAsync(() -> System.out.println("Task 1")),
    CompletableFuture.runAsync(() -> System.out.println("Task 2")),
    CompletableFuture.runAsync(() -> System.out.println("Task 3"))
);

// .join() waits for all tasks to complete
allTasks.join();

System.out.println("All tasks completed.");
```

After `.join()` completes, "All tasks completed." will be printed, ensuring that all asynchronous tasks finished first.